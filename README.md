# Vinland Solutions MudDotNet Software

[![License](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/license/mit.svg)](https://opensource.org/licenses/MIT)
[![Platform](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/platform/net/5.svg)](https://dotnet.microsoft.com/en-us/)
[![Repository](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/repository.svg)](https://gitlab.com/vinlandsolutions/muddotnet)
[![Releases](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/releases.svg)](https://gitlab.com/vinlandsolutions/muddotnet/-/releases)
[![Documentation](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/documentation.svg)](https://vinlandsolutions.gitlab.io/muddotnet/)  
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.MudDotNet/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.MudDotNet/)
[![Pipeline](https://gitlab.com/vinlandsolutions/muddotnet/badges/master/pipeline.svg)](https://gitlab.com/vinlandsolutions/muddotnet/commits/master)
[![Coverage](https://gitlab.com/vinlandsolutions/muddotnet/badges/master/coverage.svg)](https://vinlandsolutions.gitlab.io/muddotnet/reports/index.html)

**This project is in ![alpha](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/stage/alpha.svg) development and not yet fit for general use.**
