﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MudDotNet
{
    public class Logger
    {
        public static Logger Default { get; set; } = new Logger();

        public Logger() : this(Console.Out) { }

        public Logger(TextWriter writer) : this(writer, new string(' ', 4)) { }

        public Logger(TextWriter writer, string indentText)
        {
            this.Writer = writer;
            this.IndentText = indentText;
        }

        public TextWriter Writer { get; set; }

        public string IndentText { get; set; }

        public int IndentLevel { get; set; }

        public Logger IncIndentLevel()
        { this.IndentLevel++; return this; }

        public Logger DecIndentLevel()
        { this.IndentLevel--; return this; }

        public Logger ResetIndentLevel()
        { this.IndentLevel = 0; return this; }

        public Logger Flush()
        { this.Writer.Flush(); return this; }

        public Logger Write(char text)
        { this.Writer.Write(text); return this; }

        public Logger Write(ReadOnlySpan<char> text)
        { this.Writer.Write(text); return this; }

        public Logger Write(string format, params object[] args)
        { this.Writer.Write(format, args); return this; }

        public Logger WriteLine(ReadOnlySpan<char> text)
        { this.Writer.WriteLine(text); return this; }

        public Logger WriteLine(string format, params object[] args)
        { this.Writer.WriteLine(format, args); return this; }

        public Logger Begin()
        { for (int _ = 0; _ < this.IndentLevel; _++) { this.Write(this.IndentText); } return this; }

        public Logger End() => this.Write('\n');

        public Logger Log(ReadOnlySpan<char> text)
            => this.Begin().Write(text).End();

        public Logger Log(string format, params object[] args)
            => this.Begin().Write(format, args).End();

        public Logger Log0(ReadOnlySpan<char> text)
            => this.Write(text).End();

        public Logger Log0(string format, params object[] args)
            => this.Write(format, args).End();

        public Logger LogInc(ReadOnlySpan<char> text)
            => this.Log(text).IncIndentLevel();

        public Logger LogInc(string format, params object[] args)
            => this.Log(format, args).IncIndentLevel();

        public Logger LogDec(ReadOnlySpan<char> text)
            => this.DecIndentLevel().Log(text);

        public Logger LogDec(string format, params object[] args)
            => this.DecIndentLevel().Log(format, args);


    }
}
