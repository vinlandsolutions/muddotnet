﻿using System;
using System.Collections.Generic;
using System.Text;
using MudDotNet.Networking;
using MudDotNet.Systems;

namespace MudDotNet
{
    public readonly struct ContextData
    {
        public ContextData(SystemInfo systemInfo, IServer server, IAccountManager accountManager, ISystemManager systemManager)
        {
            this.SystemInfo = systemInfo;
            this.Server = server;
            this.AccountManager = accountManager;
            this.SystemManager = systemManager;
        }

        public SystemInfo SystemInfo { get; }

        public IServer Server { get; }

        public IList<IClient> Clients => this.Server.Clients;

        public IAccountManager AccountManager { get; }

        public ISystemManager SystemManager { get; }

        public Logger Logger => this.Server.Logger;
    }
}
