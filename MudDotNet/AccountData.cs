﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using CommonCore;
using CommonCore.IO;
using CommonCore.Security.Passwords;
using Newtonsoft.Json;

namespace MudDotNet
{
    public readonly struct AccountData : IEquatable<AccountData>
    {
        public static AccountData None { get; } = default;

        public static AccountData Create(string alias, SecureString secret)
        {
            ThrowHelper.IfArgNullOrEmpty(alias, nameof(alias));
            ThrowHelper.IfArgNullOrEmpty(secret, nameof(secret));
            return new AccountData(alias, PasswordHelper.GenerateHash(secret).ToString(), DateTime.UtcNow);
        }

        public AccountData(string alias, string hash, DateTime creation)
        {
            this.Alias = alias;
            this.Hash = hash;
            this.Creation = creation;
        }

        public string Alias { get; }

        public string Hash { get; }

        public DateTime Creation { get; }

        public bool IsNone() => this == AccountData.None;

        public bool IsMatch(ReadOnlySpan<char> alias)
            => alias.SequenceEqual(this.Alias);

        public bool IsMatch(SecureString secret)
            => PasswordHelper.CompareHash(secret, this.Hash);

        public bool IsMatch(ReadOnlySpan<char> alias, SecureString secret)
            => this.IsMatch(alias) && this.IsMatch(secret);

        public override string ToString()
            => string.Format("{0}|{1}|{2:O}", this.Alias, this.Hash, this.Creation);

        public override int GetHashCode()
            => HashCode.Combine(this.Alias, this.Hash, this.Creation);

        public override bool Equals(object obj)
            => obj is AccountData data && this.Equals(data);

        public bool Equals(AccountData other)
            => this.Alias == other.Alias && this.Hash == other.Hash && this.Creation == other.Creation;

        public static bool operator ==(AccountData left, AccountData right) => left.Equals(right);

        public static bool operator !=(AccountData left, AccountData right) => !(left == right);
    }

    public interface IAccountManager
    {
        IReadOnlyList<AccountData> Accounts { get; }

        int Count { get; }

        AccountData Add(string alias, SecureString secret);

        AccountData Find(ReadOnlySpan<char> alias);

        AccountData Find(ReadOnlySpan<char> alias, SecureString secret);

        IAccountManager Clear();

        IAccountManager Load(FileInfo file);

        IAccountManager Save(FileInfo file);
    }

    public class AccountManager : IAccountManager
    {
        public AccountManager()
        {
            this._accounts = new List<AccountData>();
            this.Accounts = this._accounts.AsReadOnly();
        }

        public IReadOnlyList<AccountData> Accounts { get; }
        private readonly List<AccountData> _accounts;

        public int Count => this.Accounts.Count;

        public AccountData Add(string alias, SecureString secret)
        {
            var account = AccountData.Create(alias, secret);
            this._accounts.Add(account);
            return account;
        }

        public AccountData Find(ReadOnlySpan<char> alias)
        {
            foreach (var account in this.Accounts) { if (account.IsMatch(alias)) { return account; } }
            return AccountData.None;
        }

        public AccountData Find(ReadOnlySpan<char> alias, SecureString secret)
        {
            foreach (var account in this.Accounts) { if (account.IsMatch(alias, secret)) { return account; } }
            return AccountData.None;
        }

        public IAccountManager Clear()
        {
            this._accounts.Clear();
            return this;
        }

        public IAccountManager Load(FileInfo file)
        {
            this.Clear();
            if (file.Exists)
            {
                var accounts = JsonConvert.DeserializeObject<List<AccountData>>(file.ReadAllText());
                if (accounts.Count > 0) { this._accounts.AddRange(accounts); }
            }
            return this;
        }

        public IAccountManager Save(FileInfo file)
        {
            File.WriteAllText(file.FullName, JsonConvert.SerializeObject(this.Accounts, Formatting.Indented));
            return this;
        }
    }

    public static class IAccountManagerExtensions
    {
        public static AccountData Find(this IAccountManager self, ReadOnlySpan<char> alias, ReadOnlySpan<char> secret)
        {
            using var secure = new SecureString();
            foreach (var c in secret) { secure.AppendChar(c); }
            return self.Find(alias, secure);
        }
    }
}
