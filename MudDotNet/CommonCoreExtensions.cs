﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CommonCore;
using CommonCore.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MudDotNet
{
    public static class CommonCoreExtensions
    {

        /// <inheritdoc cref="File.WriteAllBytes(string, byte[])" />
        public static void WriteAllBytes(this FileInfo self, byte[] bytes)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllBytes(self.FullName, bytes);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, string[])" />
        public static void WriteAllLines(this FileInfo self, string[] contents)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, string[], Encoding)" />
        public static void WriteAllLines(this FileInfo self, string[] contents, Encoding encoding)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents, encoding);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, IEnumerable{string})" />
        public static void WriteAllLines(this FileInfo self, IEnumerable<string> contents)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, IEnumerable{string}, Encoding)" />
        public static void WriteAllLines(this FileInfo self, IEnumerable<string> contents, Encoding encoding)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents, encoding);
        }

        /// <inheritdoc cref="File.WriteAllText(string, string)" />
        public static void WriteAllText(this FileInfo self, string contents)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllText(self.FullName, contents);
        }

        /// <inheritdoc cref="File.WriteAllText(string, string, Encoding)" />
        public static void WriteAllText(this FileInfo self, string contents, Encoding encoding)
        {
            ThrowHelper.IfSelfNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllText(self.FullName, contents, encoding);
        }

        //public static void FindLineSeg(this StringBuilder self, int lineIndex, out int index, out int length)
        //{
        //    if (self.Length == 0)
        //    {
        //        index = (lineIndex == 0 ? 0 : -1);
        //        length = 0;
        //        return;
        //    }


        //    index = 0;
        //    length = 0;
        //    int loopIndex = 0;
        //    while (loopIndex < lineIndex)
        //    {
        //        index = self.IndexOfAny(new[] { '\r', '\n' }, index, ECharComparison.CaseSensitive);
        //        loopIndex++;
        //    }
        //}

        //public static StringBuilder RemoveLines(this StringBuilder self, int lineStartIndex, int maxCount)
        //{
        //    ThrowHelper.IfSelfNull(self);
        //    ThrowHelper.IfArgLesser(lineStartIndex, nameof(lineStartIndex), 0);
        //    ThrowHelper.IfArgLesser(maxCount, nameof(maxCount), 0);
        //    if (self.Length == 0 || maxCount == 0) { return self; }

        //    int prev = 0;
        //    int lineCount = 0;

        //    for (int index = 0; index < self.Length; index++)
        //    {
        //        char c = self[index];

        //        if (c == '\n')
        //        {
        //        }
        //    }









        //    for (int index = 0; index < self.Length; index++)
        //    {
        //        char c = self[index];

        //        if (c == '\n')
        //        {
        //            if (self.IsNextChar(index, '\r')) { index++; }
        //            if (lineIndex == lineCount)
        //            {
        //                self.Remove(prev, index - prev + 1);
        //                return self;
        //            }
        //            lineCount++;
        //            prev = index + 1;
        //            continue;
        //        }
        //        else if (c == '\r')
        //        {
        //            if (self.IsNextChar(index, '\n')) { index++; }
        //            if (lineIndex == lineCount)
        //            {
        //                self.Remove(prev, index - prev + 1);
        //                return self;
        //            }
        //            lineCount++;
        //            prev = index + 1;
        //            continue;
        //        }
        //    }
        //    if (lineIndex == lineCount && prev <= self.Length)
        //    {
        //        if (self.IsPrevChar(prev, '\n'))
        //        {
        //            prev--;
        //            if (self.IsPrevChar(prev, '\r'))
        //            { prev--; }
        //        }
        //        else if (self.IsPrevChar(prev, '\r'))
        //        {
        //            prev--;
        //            if (self.IsPrevChar(prev, '\n'))
        //            { prev--; }
        //        }

        //        if (prev < self.Length)
        //        { self.Remove(prev, self.Length - prev); }
        //    }
        //    return self;
        //}
    }

    public class FileSystemInfoConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => typeof(FileSystemInfo).IsAssignableFrom(objectType);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            => reader.TokenType == JsonToken.Null ? null : Activator.CreateInstance(objectType, reader.Value.ToString());

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => writer.WriteValue(value.ToString());
    }
}
