﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CommonCore.IO;
using Newtonsoft.Json;

namespace MudDotNet
{
    public class SystemInfo
    {
        public static SystemInfo FromJsonFile(FileInfo file)
            => !file.Exists ? new SystemInfo() : JsonConvert.DeserializeObject<SystemInfo>(file.ReadAllText());

        public SystemInfo() { }

        [JsonConstructor]
        public SystemInfo(string title, int portNumber, int backlogSize, DirectoryInfo dataDirectory)
        {
            this.Title = title;
            this.PortNumber = portNumber;
            this.BacklogSize = backlogSize;
            this.DataDirectory = dataDirectory;
        }

        public string Title { get; } = "Server";

        public int PortNumber { get; } = 4000;

        public int BacklogSize { get; } = 20;

        public DirectoryInfo DataDirectory { get; } = new DirectoryInfo("./Data");

        public DirectoryInfo SystemsDirectory { get; } = new DirectoryInfo("./Systems");

        public void ToJsonFile(FileInfo file)
            => file.WriteAllText(JsonConvert.SerializeObject(this, Formatting.Indented));
    }
}
