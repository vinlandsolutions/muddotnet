﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;

namespace MudDotNet.Networking
{
    public readonly struct PuebloData
    {
        public static PuebloData Empty { get; } = new PuebloData();

        public PuebloData(string version, string hash)
        {
            this.Version = version;
            this.Hash = hash;
        }

        public readonly string Version { get; }

        public readonly string Hash { get; }

        public readonly bool IsEmpty() => this == PuebloData.Empty;

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        public readonly override int GetHashCode() => HashCode.Combine(this.Version, this.Hash);

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">Another object to compare to.</param>
        public readonly override bool Equals(object? obj) => obj is PuebloData data && this.Equals(data);

        /// <summary>
        /// Indicates whether this instance and a specified PuebloData are equal.
        /// </summary>
        /// <param name="other">Another PuebloData to compare to.</param>
        public readonly bool Equals(PuebloData other) => this.Hash == other.Hash;

        /// <summary>
        /// Indicates whether two PuebloData are equal.
        /// </summary>
        /// <param name="left">Left PuebloData to compare.</param>
        /// <param name="right">Right PuebloData to compare.</param>
        public static bool operator ==(PuebloData left, PuebloData right) => left.Equals(right);

        /// <summary>
        /// Indicates whether two PuebloData are not equal.
        /// </summary>
        /// <param name="left">Left PuebloData to compare.</param>
        /// <param name="right">Right PuebloData to compare.</param>
        public static bool operator !=(PuebloData left, PuebloData right) => !left.Equals(right);
    }
}
