﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Linq;
using System.Text;
using CommonCore;

namespace MudDotNet.Networking
{
    using static ETelnet;

    public readonly struct MxpPacket
    {
        public static MxpPacket Empty { get; } = default;

        public static MxpPacket MxpWill { get; } = new MxpPacket(IAC, WILL, MXP);
        public static MxpPacket MxpDont { get; } = new MxpPacket(IAC, DONT, MXP);
        public static MxpPacket MxpDo { get; } = new MxpPacket(IAC, DO, MXP);
        public static MxpPacket MxpWont { get; } = new MxpPacket(IAC, WONT, MXP);
        public static MxpPacket MxpStart { get; } = new MxpPacket(IAC, SB, MXP, IAC, SE);

        public MxpPacket(params byte[] codes)
        {
            Throw.If.Arg.Null(nameof(codes), codes);

            this.Bytes = new byte[codes.Length];
            codes.CopyTo(this.Bytes, 0);
            //for (int index = 0; index < codes.Length; index++) { this.Bytes[index] = codes[index]; }
            this.Text = Encoding.Default.GetString(this.Bytes);
        }

        public MxpPacket(params ETelnet[] codes)
        {
            Throw.If.Arg.Null(nameof(codes), codes);

            this.Bytes = new byte[codes.Length];
            codes.CopyTo(this.Bytes, 0);
            //for (int index = 0; index < codes.Length; index++) { this.Bytes[index] = (byte)codes[index]; }
            this.Text = Encoding.Default.GetString(this.Bytes);
        }

        public readonly byte[] Bytes { get; }

        public readonly string Text { get; }

        public readonly bool IsEmpty() => this == MxpPacket.Empty;

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        public readonly override int GetHashCode() => HashCode.Combine(this.Bytes, this.Text);

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">Another object to compare to.</param>
        public readonly override bool Equals(object? obj) => obj is MxpPacket data && this.Equals(data);

        /// <summary>
        /// Indicates whether this instance and a specified Temp are equal.
        /// </summary>
        /// <param name="other">Another Temp to compare to.</param>
        public readonly bool Equals(MxpPacket other) => this.Bytes.SequenceEqual(other.Bytes);

        /// <summary>
        /// Indicates whether two Temp are equal.
        /// </summary>
        /// <param name="left">Left Temp to compare.</param>
        /// <param name="right">Right Temp to compare.</param>
        public static bool operator ==(MxpPacket left, MxpPacket right) => left.Equals(right);

        /// <summary>
        /// Indicates whether two Temp are not equal.
        /// </summary>
        /// <param name="left">Left Temp to compare.</param>
        /// <param name="right">Right Temp to compare.</param>
        public static bool operator !=(MxpPacket left, MxpPacket right) => !left.Equals(right);

        public static implicit operator byte[](MxpPacket mxp) => mxp.Bytes;
    }
}
