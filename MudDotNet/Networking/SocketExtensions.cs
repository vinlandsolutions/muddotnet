﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Net.Sockets;
using CommonCore;

namespace MudDotNet.Networking
{
    public static class SocketExtensions
    {

        public static bool TryAccept(this Socket self, out Socket socket)
        {
            ThrowHelper.IfSelfNull(self);
            try { socket = self.Accept(); return true; }
            catch (SocketException) { socket = null; return false; }
        }

        public static void SetKeepAlive(this Socket self, bool on, uint keepAliveTime, uint keepAliveInterval)
        {
            ThrowHelper.IfSelfNull(self);
            int size = System.Runtime.InteropServices.Marshal.SizeOf(new uint());

            var inOptionValues = new byte[size * 3];

            BitConverter.GetBytes((uint)(on ? 1 : 0)).CopyTo(inOptionValues, 0);
            BitConverter.GetBytes((uint)keepAliveTime).CopyTo(inOptionValues, size);
            BitConverter.GetBytes((uint)keepAliveInterval).CopyTo(inOptionValues, size * 2);

            self.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);
        }
    }
}
