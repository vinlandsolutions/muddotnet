﻿using System;

namespace MudDotNet.Networking
{
    public class ClientEventArgs : EventArgs
    {
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        public static ClientEventArgs Empty { get; } = new ClientEventArgs();
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword

        private ClientEventArgs() { }

        public ClientEventArgs(IClient client) => this.Client = client;

        public IClient Client { get; } = null;
    }
}
