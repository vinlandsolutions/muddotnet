﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace MudDotNet.Networking
{
    public interface IClient : IDisposable
    {
        uint ID { get; }

        bool Active { get; }

        bool Disconnected { get; }

        ConnectionData Connection { get; }

        AccountData Account { get; set; }

        DateTime IdleStamp { get; }

        TimeSpan IdleTime { get; }

        PuebloData Pueblo { get; set; }

        bool MxpEnabled { get; }

        void Disconnect();

        void Send(byte[] bytes);

        void Send(string text);

        void SendLine(string text);

        void Send(ReadOnlySpan<char> text);

        void SendLine(ReadOnlySpan<char> text);

        void Send(StringBuilder content);

        void SendLine(StringBuilder content);

        void Send(FileInfo content);

        void SendLine(FileInfo content);

        void Send(IEnumerable<char> text);

        void SendLine(IEnumerable<char> text);
    }
}
