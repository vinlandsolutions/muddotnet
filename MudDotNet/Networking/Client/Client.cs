﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using CommonCore;

namespace MudDotNet.Networking
{
    public class Client : IClient
    {
        private static readonly object Lock = new object();

        public Client(uint id, Server server, Socket socket)
        {
            this.ID = id;
            this.Server = server;
            this.Socket = socket;
            this.Socket.SetKeepAlive(true, 1000, 1000);
            this.TextReader = new StreamReader(new NetworkStream(this.Socket, false));
            this.TextWriter = new StreamWriter(new NetworkStream(this.Socket, false));
            this.BinReader = new BinaryReader(new NetworkStream(this.Socket, false));
            this.BinWriter = new BinaryWriter(new NetworkStream(this.Socket, false));

            this.Connection = new ConnectionData(this.Socket.RemoteEndPoint as IPEndPoint);
            this.IdleStamp = this.Connection.Stamp;

            this.Thread = new Thread(this.ClientLoop);
            this.Thread.Start();
        }

        public uint ID { get; }

        public IServer Server { get; protected set; }

        public Socket Socket { get; protected set; }

        public StreamReader TextReader { get; protected set; }

        public StreamWriter TextWriter { get; protected set; }

        public BinaryReader BinReader { get; protected set; }

        public BinaryWriter BinWriter { get; protected set; }

        public Thread Thread { get; protected set; }

        public bool Active { get; protected set; }

        public bool Disconnected { get; protected set; }

        public ConnectionData Connection { get; }

        public AccountData Account { get; set; }

        public DateTime IdleStamp { get; protected set; }

        public TimeSpan IdleTime => DateTime.UtcNow - this.IdleStamp;

        public PuebloData Pueblo { get; set; }

        public bool MxpEnabled { get; protected set; }

        public void Disconnect() { lock (Client.Lock) { this.Active = false; } }

        private void ClientLoop()
        {
            lock (Client.Lock) { this.Active = true; }
            this.Send(MxpPacket.MxpWill);
            var dummy = new byte[1];
            while (this.Active)
            {
                bool negotiation = true;
                while (this.Active)
                {
                    string line = null;
                    try
                    {
                        int bytesRead = this.Socket.Receive(dummy, SocketFlags.Peek);
                        if (bytesRead == 0) { this.Disconnect(); break; }

                        if (negotiation)
                        {
                            if (dummy[0] == (byte)ETelnet.IAC)
                            {
                                byte[] bytes = new byte[3];
                                this.Socket.Receive(bytes, SocketFlags.None);
                                //var data = new MxpPacket(this.BinReader.ReadByte(), this.BinReader.ReadByte(), this.BinReader.ReadByte());
                                var data = new MxpPacket(bytes[0], bytes[1], bytes[2]);
                                if (data == MxpPacket.MxpDont)
                                {
                                    this.MxpEnabled = false;
                                }
                                else if (data == MxpPacket.MxpDo)
                                {
                                    this.MxpEnabled = true;
                                    this.Send(MxpPacket.MxpStart);
                                    if (File.Exists("./Data/CustomElements.mxp"))
                                    { this.Send("\x1B[6z" + System.IO.File.ReadAllText("./Data/CustomElements.mxp") + "\x1B[7z"); }
                                }
                            }
                            negotiation = false;
                        }

                        line = this.TextReader.ReadLine();
                    }
                    catch (IOException exception) { }
                    if (!this.Active) { break; }
                    if (line.IsNotEmpty())
                    {
                        this.Server.HandleInput(this, new StringBuilder(line));
                        this.IdleStamp = DateTime.UtcNow;
                    }
                }
            }
            lock (Client.Lock) { this.Disconnected = true; }
        }



        public void Send(byte[] bytes)
        {
            if (this.BinWriter == null || bytes.IsEmpty()) { return; }
            this.BinWriter.Write(bytes);
        }

        public void Send(string text)
        {
            if (this.TextWriter == null || text.IsEmpty()) { return; }
            this.TextWriter.Write(text);
            this.TextWriter.Flush();
        }

        public void SendLine(string text)
        {
            if (this.TextWriter == null || text.IsEmpty()) { return; }
            this.TextWriter.Write(text);
            this.TextWriter.Write('\n');
            this.TextWriter.Flush();
        }

        public void Send(ReadOnlySpan<char> text)
        {
            if (this.TextWriter == null || text.IsEmpty) { return; }
            this.TextWriter.Write(text);
            this.TextWriter.Flush();
        }

        public void SendLine(ReadOnlySpan<char> text)
        {
            if (this.TextWriter == null || text.IsEmpty) { return; }
            this.TextWriter.Write(text);
            this.TextWriter.Write('\n');
            this.TextWriter.Flush();
        }

        public void Send(StringBuilder text)
        {
            if (this.TextWriter == null || text.IsEmpty()) { return; }
            for (int index = 0; index < text.Length; index++)
            { this.TextWriter.Write(text[index]); }
            this.TextWriter.Flush();
        }

        public void SendLine(StringBuilder text)
        {
            if (this.TextWriter == null || text.IsEmpty()) { return; }
            for (int index = 0; index < text.Length; index++)
            { this.TextWriter.Write(text[index]); }
            this.TextWriter.Write('\n');
            this.TextWriter.Flush();
        }

        public void Send(FileInfo info)
        {
            if (this.TextWriter == null || !info.Exists) { return; }
            using (var reader = new StreamReader(info.FullName, Encoding.UTF8))
            {
                int value;
                while ((value = reader.Read()) != -1)
                { this.TextWriter.Write((char)value); }
            }
            this.TextWriter.Flush();
        }

        public void SendLine(FileInfo info)
        {
            if (this.TextWriter == null || !info.Exists) { return; }
            using (var reader = new StreamReader(info.FullName, Encoding.UTF8))
            {
                int value;
                while ((value = reader.Read()) != -1)
                { this.TextWriter.Write((char)value); }
            }
            this.TextWriter.Write('\n');
            this.TextWriter.Flush();
        }

        public void Send(IEnumerable<char> text)
        {
            if (this.TextWriter == null || text.IsEmpty()) { return; }
            foreach (var c in text) { this.TextWriter.Write(c); }
            this.TextWriter.Flush();
        }

        public void SendLine(IEnumerable<char> text)
        {
            if (this.TextWriter == null || text.IsEmpty()) { return; }
            foreach (var c in text) { this.TextWriter.Write(c); }
            this.TextWriter.Write('\n');
            this.TextWriter.Flush();
        }

        #region IDisposable Support

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating if the current <see cref="ClientBus"/> instance
        /// has been disposed.
        /// </summary>
        public bool Disposed { get; protected set; } = false;

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose() => this.Dispose(true);

        /// <summary>
        /// Releases all resources used by the current instance of the <see cref="ClientBus"/> class.
        /// </summary>
        /// <param name="disposing">Indicate whether managed resources should be disposed.</param>
        protected virtual void Dispose(bool disposing)
        {
            lock (Client.Lock)
            {
                if (!this.Disposed)
                {
                    if (disposing)
                    {
                        if (this.Socket != null)
                        {
                            try { this.Socket.Shutdown(SocketShutdown.Both); } catch (SocketException) { }
                            this.TextReader.Dispose(); this.TextReader = null;
                            this.TextWriter.Dispose(); this.TextWriter = null;
                            this.BinReader.Dispose(); this.BinReader = null;
                            this.BinWriter.Dispose(); this.BinWriter = null;
                            this.Socket.Dispose(); this.Socket = null;
                        }
                    }
                    this.Disposed = true;
                }
            }
        }

        #endregion

    }
}
