﻿using System;
using System.Text;

namespace MudDotNet.Networking
{
    public class ClientInputEventArgs : EventArgs
    {
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        public static ClientInputEventArgs Empty { get; } = new ClientInputEventArgs();
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword

        private ClientInputEventArgs() { }

        public ClientInputEventArgs(IClient client, StringBuilder input)
        {
            this.Client = client;
            this.Input = input;
        }

        public IClient Client { get; } = null;

        public StringBuilder Input { get; } = null;
    }
}
