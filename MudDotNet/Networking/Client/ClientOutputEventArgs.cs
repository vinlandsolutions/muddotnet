﻿using System;
using System.Text;

namespace MudDotNet.Networking
{
    public class ClientOutputEventArgs : EventArgs
    {
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        public static ClientOutputEventArgs Empty { get; } = new ClientOutputEventArgs();
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword

        private ClientOutputEventArgs() { }

        public ClientOutputEventArgs(IClient sender, IClient receiver, StringBuilder content)
        {
            this.Sender = sender;
            this.Receiver = receiver;
            this.Content = content;
        }

        public IClient Sender { get; } = null;

        public IClient Receiver { get; } = null;

        public IClient Client { get; } = null;

        public StringBuilder Content { get; } = null;
    }
}
