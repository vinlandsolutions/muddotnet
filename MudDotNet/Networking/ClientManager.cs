﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Text;

//namespace MudDotNet.Networking
//{
//    public class ClientManager : IReadOnlyList<Client>
//    {
//        private static readonly object Lock = new object();

//        public ClientManager() : this(Logger.Default) { }

//        public ClientManager(Logger log) => this.Logger = log;

//        private readonly List<Client> _items = new List<Client>();

//        public Logger Logger { get; }

//        public int Count => this._items.Count;

//        public Client this[int index] => this._items[index];

//        public IEnumerator<Client> GetEnumerator() => ((IReadOnlyList<Client>)this._items).GetEnumerator();

//        IEnumerator IEnumerable.GetEnumerator() => ((IReadOnlyList<Client>)this._items).GetEnumerator();

//        public void Add(Client client) => this._items.Add(client);

//        public void DisconnectAll()
//        { foreach (var client in this) { if (client.Active) { client.Disconnect(); } } }

//        public void CleanDead()
//        {
//            for (int index = this.Count - 1; index > -1; index--)
//            {
//                var client = this[index];
//                Debug.Assert(client != null);
//                //if (client == null) { lock (ClientManager.Lock) { this._items.RemoveAt(index); } }
//                //else
//                if (!client.Active && client.Disconnected)
//                {
//                    lock (ClientManager.Lock) { this._items.RemoveAt(index); }
//                    this.Logger.Log("Client Disconnected: #{0} ({1}) | {2}:{3}", client.ID, this.Count, client.Connection.IP, client.Connection.Port);
//                    //this.OnClientDisconnected(new ClientEventArgs(client));
//                    client.Dispose();
//                }
//            }
//        }
//    }
//}
