﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace MudDotNet.Networking
{
    public enum ETelnet : byte
    {
        Echo = 1,
        SuppressGoAhead = 3,
        Status = 5,
        TimingMark = 6,
        TerminalType = 24,
        NAWS = 31,
        TerminalSpeed = 32,
        RemoteFlowControl = 33,
        LineMode = 34,
        EnvironmentVariables = 36,

        // MUD Protocols

        MSDP = 69,
        // Mud eXtension Protocol
        MXP = 91,// 0x5B
        MCCP1 = 85,
        MCCP2 = 86,
        MSP = 90,
        
        // Sub-negotiation End
        SE = 240,
        NoOperation = 241,
        DataMark = 242,
        Break = 243,
        InterruptProcess = 244,
        AbortOutput = 245,
        AreYouThere = 246,
        EraseCharacter = 247,
        EraseLine = 248,
        GoAhead = 249,
        // Sub-negotiation Begin
        SB = 250,
        
        WILL = 251,// 0xFB
        WONT = 252,
        DO = 253,
        DONT = 254,

        // Interpret As Command
        IAC = 255// 0xFF
    };
}
