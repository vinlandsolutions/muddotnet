﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using CommonCore;

namespace MudDotNet.Networking
{
    public class Server : IServer
    {
        private static readonly object Lock = new object();

        public Server() : this(Logger.Default) { }

        public Server(Logger log) => this.Logger = log;

        public Logger Logger { get; }

        public bool Active { get; protected set; }

        public bool Running { get; protected set; }

        public bool Initialized { get; protected set; }

        public ConnectionData Connection { get; protected set; }

        public IList<IClient> Clients { get; } = new List<IClient>();

        protected Socket Socket { get; set; }

        public uint FindNextID()
        {
            lock (Server.Lock)
            {
                for (uint id = 0; id < uint.MaxValue; id++)
                {
                    bool found = false;
                    foreach (var client in this.Clients)
                    { if (id == client.ID) { found = true; } }
                    if (found) { continue; }
                    else { return id; }
                }
                throw new InvalidOperationException("No free ids found.");
            }
        }

        protected async void AcceptConnectionsAsync() => await Task.Run(() => this.AcceptConnections());

        protected void AcceptConnections()
        {
            while (this.Active)
            {
                if (this.Socket.TryAccept(out Socket socket))
                {
                    var client = new Client(this.FindNextID(), this, socket);
                    this.Clients.Add(client);
                    this.Logger.Log("Client Connected: #{0} ({1}) | {2}:{3}", client.ID, this.Clients.Count, client.Connection.IP, client.Connection.Port);
                    this.OnClientConnected(new ClientEventArgs(client));
                }
            }
        }

        public void Initialize()
        {
            if (!this.Initialized)
            {
                this.OnServerInitialize(ServerEventArgs.Empty);

                this.Logger.LogInc("Server Initializing");
                this.OnServerInitializing(ServerEventArgs.Empty);

                lock (Server.Lock) { this.Initialized = true; }
                this.Logger.LogDec("Server Initialized");
                this.OnServerInitialized(ServerEventArgs.Empty);
            }
        }

        public void Destroy()
        {
            if (this.Initialized)
            {
                this.OnServerDestroy(ServerEventArgs.Empty);

                this.Logger.LogInc("Server Destroying");
                this.OnServerDestroying(ServerEventArgs.Empty);

                lock (Server.Lock) { this.Initialized = true; }
                this.Logger.LogDec("Server Destroyed");
                this.OnServerDestroyed(ServerEventArgs.Empty);
            }
        }

        public async void StartAsync(int port, int backlogSize) => await Task.Run(() => this.Start(port, backlogSize));

        public void Start(int port, int backlogSize)
        {
            lock (Server.Lock) { this.Running = true; }

            this.Initialize();

            lock (Server.Lock) { this.Active = true; }

            this.Logger.LogInc("Server Starting");
            this.Logger.Log("Creating Socket");
            this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.Socket.Bind(new IPEndPoint(IPAddress.Any, port));
            this.Connection = new ConnectionData(this.Socket.LocalEndPoint as IPEndPoint, null, DateTime.UtcNow);
            this.Socket.Listen(backlogSize);
            this.Logger.Log("Listening on port " + port);
            this.AcceptConnectionsAsync();
            this.Logger.Log("Accepting Connections");
            this.Logger.LogDec("Server Started");
            this.OnServerStarted(ServerEventArgs.Empty);

            while (this.Active || this.Clients.Count > 0)
            {
                if (!this.Active) { this.DisconnectAllClients(); }
                this.CleanDeadClients();
                //for (int index = this.Clients.Count - 1; index > -1; index--)
                //{
                //    var client = this.Clients[index];
                //    Debug.Assert(client != null);
                //    //if (client == null) { lock (Server.Lock) { this.Clients.RemoveAt(index); } }
                //    //else
                //    if (!this.Active && client.Active) { client.Disconnect(); }
                //    else if (!client.Active && client.Disconnected)
                //    {
                //        lock (Server.Lock) { this.Clients.RemoveAt(index); }
                //        this.Logger.Log("Client Disconnected: #{0} ({1}) | {2}:{3}", client.ID, this.Clients.Count, client.Connection.IP, client.Connection.Port);
                //        this.OnClientDisconnected(new ClientEventArgs(client));
                //        client.Dispose();
                //    }
                //}
            }

            this.Logger.LogInc("Server Stopping");

            this.Logger.Log("Destroying Socket");
            this.Socket.Dispose();

            this.Logger.LogDec("Server Stopped");
            this.OnServerStopped(ServerEventArgs.Empty);

            this.Destroy();

            lock (Server.Lock) { this.Running = false; }
        }

        public void Stop() => this.Active = false;

        public void HandleInput(IClient client, StringBuilder input)
        {
            if (input.IsEmpty()) { return; }
            this.OnClientInput(new ClientInputEventArgs(client, input));
        }

        public void Broadcast(IClient sender, IClient receiver, string content)
        { if (content.IsNotEmpty()) { this.Broadcast(sender, receiver, new StringBuilder(content)); } }

        public void Broadcast(IClient sender, IClient receiver, StringBuilder content)
        {
            if (content.IsEmpty()) { return; }
            this.OnClientOutput(new ClientOutputEventArgs(sender, receiver, content));
            //output.ReplaceAny(Util.PuebloReplacements);
            receiver.SendLine(content);
        }

        public void Broadcast(IClient sender, IClient receiver, FileInfo file)
        { if (file.Exists) { this.Broadcast(sender, receiver, new StringBuilder().AppendFile(file)); } }





        public void DisconnectAllClients()
        { foreach (var client in this.Clients) { if (client.Active) { client.Disconnect(); } } }

        public void CleanDeadClients()
        {
            for (int index = this.Clients.Count - 1; index > -1; index--)
            {
                var client = this.Clients[index];
                Debug.Assert(client != null);
                //if (client == null) { lock (Server.Lock) { this.Clients.RemoveAt(index); } }
                //else
                if (!client.Active && client.Disconnected)
                {
                    lock (Server.Lock) { this.Clients.RemoveAt(index); }
                    this.Logger.Log("Client Disconnected: #{0} ({1}) | {2}:{3}", client.ID, this.Clients.Count, client.Connection.IP, client.Connection.Port);
                    this.OnClientDisconnected(new ClientEventArgs(client));
                    client.Dispose();
                }
            }
        }





        #region Events and Event Handlers

        public event EventHandler<ServerEventArgs> ServerInitialize;
        protected void OnServerInitialize(ServerEventArgs e) => this.ServerInitialize?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerInitializing;
        protected void OnServerInitializing(ServerEventArgs e) => this.ServerInitializing?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerInitialized;
        protected void OnServerInitialized(ServerEventArgs e) => this.ServerInitialized?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerStarted;
        protected void OnServerStarted(ServerEventArgs e) => this.ServerStarted?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerStopped;
        protected void OnServerStopped(ServerEventArgs e) => this.ServerStopped?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerDestroy;
        protected void OnServerDestroy(ServerEventArgs e) => this.ServerDestroy?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerDestroying;
        protected void OnServerDestroying(ServerEventArgs e) => this.ServerDestroying?.Invoke(this, e);

        public event EventHandler<ServerEventArgs> ServerDestroyed;
        protected void OnServerDestroyed(ServerEventArgs e) => this.ServerDestroyed?.Invoke(this, e);

        public event EventHandler<ClientEventArgs> ClientConnected;
        protected void OnClientConnected(ClientEventArgs e) => this.ClientConnected?.Invoke(this, e);

        public event EventHandler<ClientInputEventArgs> ClientInput;
        protected void OnClientInput(ClientInputEventArgs e) => this.ClientInput?.Invoke(this, e);

        public event EventHandler<ClientOutputEventArgs> ClientOutput;
        protected void OnClientOutput(ClientOutputEventArgs e) => this.ClientOutput?.Invoke(this, e);

        public event EventHandler<ClientEventArgs> ClientUpdated;
        protected void OnClientUpdated(ClientEventArgs e) => this.ClientUpdated?.Invoke(this, e);

        public event EventHandler<ClientEventArgs> ClientDisconnected;
        protected void OnClientDisconnected(ClientEventArgs e) => this.ClientDisconnected?.Invoke(this, e);

        #endregion

        #region IDisposable Support

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating if the current <see cref="ClientBus"/> instance
        /// has been disposed.
        /// </summary>
        public bool Disposed { get; protected set; } = false;

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose() => this.Dispose(true);

        /// <summary>
        /// Releases all resources used by the current instance of the <see cref="ClientBus"/> class.
        /// </summary>
        /// <param name="disposing">Indicate whether managed resources should be disposed.</param>
        protected virtual void Dispose(bool disposing)
        {
            lock (Server.Lock)
            {
                if (!this.Disposed)
                {
                    if (disposing)
                    {
                        if (this.Socket != null)
                        {
                            try { this.Socket.Shutdown(SocketShutdown.Both); } catch (Exception) { }
                            this.Socket.Dispose(); this.Socket = null;
                        }
                    }
                    this.Disposed = true;
                }
            }
        }

        #endregion

    }
}
