﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MudDotNet.Networking
{
    public interface IServer : IDisposable
    {
        Logger Logger { get; }

        bool Active { get; }

        bool Running { get; }

        bool Initialized { get; }

        ConnectionData Connection { get; }

        IList<IClient> Clients { get; }

        void Stop();

        void Start(int port, int backlogSize);

        void StartAsync(int port, int backlogSize);

        void Broadcast(IClient sender, IClient receiver, string content);

        void Broadcast(IClient sender, IClient receiver, StringBuilder content);

        void Broadcast(IClient sender, IClient receiver, FileInfo file);

        void HandleInput(IClient client, StringBuilder input);

        event EventHandler<ServerEventArgs> ServerInitialize;

        event EventHandler<ServerEventArgs> ServerInitializing;

        event EventHandler<ServerEventArgs> ServerInitialized;

        event EventHandler<ServerEventArgs> ServerStarted;

        event EventHandler<ServerEventArgs> ServerStopped;

        event EventHandler<ServerEventArgs> ServerDestroy;

        event EventHandler<ServerEventArgs> ServerDestroying;

        event EventHandler<ServerEventArgs> ServerDestroyed;

        event EventHandler<ClientEventArgs> ClientConnected;

        event EventHandler<ClientInputEventArgs> ClientInput;

        event EventHandler<ClientOutputEventArgs> ClientOutput;

        event EventHandler<ClientEventArgs> ClientUpdated;

        event EventHandler<ClientEventArgs> ClientDisconnected;
    }
}
