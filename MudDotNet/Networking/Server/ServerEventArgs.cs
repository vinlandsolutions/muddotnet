﻿using System;

namespace MudDotNet.Networking
{
    public class ServerEventArgs : EventArgs
    {
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        public static ServerEventArgs Empty { get; } = new ServerEventArgs();
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword

        public ServerEventArgs()
        {
        }
    }
}
