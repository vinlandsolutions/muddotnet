﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MudDotNet.Networking
{
    public readonly struct ConnectionData
    {
        public ConnectionData(IPEndPoint endPoint)
            : this(endPoint, Dns.GetHostEntry(endPoint.Address), DateTime.UtcNow) { }

        public ConnectionData(IPEndPoint endPoint, DateTime timeStamp)
            : this(endPoint, Dns.GetHostEntry(endPoint.Address), timeStamp) { }

        public ConnectionData(IPEndPoint endPoint, IPHostEntry hostEntry, DateTime stamp)
        {
            this.EndPoint = endPoint;
            this.HostEntry = hostEntry;
            this.Stamp = stamp;
        }

        public IPEndPoint EndPoint { get; }

        public IPHostEntry HostEntry { get; }

        public DateTime Stamp { get; }

        public TimeSpan Time => DateTime.UtcNow - this.Stamp;

        public IPAddress IP => this.EndPoint.Address;

        public int Port => this.EndPoint.Port;

        public string Host => this.HostEntry.HostName;

    }
}
