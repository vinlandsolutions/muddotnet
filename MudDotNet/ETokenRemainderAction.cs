﻿namespace MudDotNet
{
    public enum ETokenRemainderAction : int
    {
        None,
        Discard,
        Join,
        Add
    }
}
