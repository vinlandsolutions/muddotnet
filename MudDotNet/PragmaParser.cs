﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonCore;

namespace MudDotNet
{
    public static class PragmaParser
    {
        //public static string[] PragmaKeys { get; } = { "id", "title", "topic", "keywords" };

        public readonly struct Doc
        {
            public Doc(IReadOnlyDictionary<string, string> pragmas, StringBuilder content)
            {
                this.Pragmas = pragmas;
                this.Content = content;
            }

            public IReadOnlyDictionary<string, string> Pragmas { get; }

            public StringBuilder Content { get; }
        }

        //public static Doc Parse(ITextSegment source)
        //{
        //    var pragmas = new Dictionary<string, string>();
        //    var content = new StringBuilder(source.Count);
        //    foreach (var line in source.ToLineSequence())
        //    {
        //    }
        //}

        public static int ParsePragmas(StringBuilder source, out List<(string key, string value)> pragmas)
        {
            pragmas = new List<(string key, string value)>();
            int count = 0;
            foreach (var line in source.ToLineSequence())
            {
                // First non-comment line breaks the loop.
                if (line.IsChar(0, '#')) { count += 1; } else { break; }

                // Skip empty, non-empty, and escape comments.
                if (line.Length == 1 || line.IsNextChar(0, ' ') || line.IsNextChar(0, '#')) { continue; }

                // Find the delimiting space which is at least third element.
                // If there is no delimiter, then it's an empty pragma.
                int space = line.IndexOf(' ', 2);
                var pragma = space == -1
                    ? (line.Substring(1), string.Empty)
                    : (line.Substring(1, space - 1), line.Substring(space + 1));
                pragmas.Add(pragma);
            }
            return count;
        }

        public static bool ValidatePragma(string variable, string key, string value, StringBuilder content)
        {
            var dupMessage = "ERROR: Duplicate pragma '{0}' values '{1}' and '{2}'";
            var emptyMessage = "ERROR: Empty pragma '{0}'";
            if (variable != null) { content.AppendFormatLine(dupMessage, key, variable, value); return false; }
            else if (value.Length == 0) { content.AppendFormatLine(emptyMessage, key); return false; }
            return true;
        }

        public static bool ValidatePragma(IEnumerable<string> variable, string key, string value, StringBuilder content)
        {
            var dupMessage = "ERROR: Duplicate pragma '{0}' values '{1}' and '{2}'";
            var emptyMessage = "ERROR: Empty pragma '{0}'";
            if (variable != null) { content.AppendFormatLine(dupMessage, key, variable.Join(", "), value); return false; }
            else if (value.Length == 0) { content.AppendFormatLine(emptyMessage, key); return false; }
            return true;
        }
    }
}
