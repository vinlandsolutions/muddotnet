﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace MudDotNet.Systems.Help
{
    public interface IHelpTopicManager : IEnumerable<HelpTopic>
    {
        Dictionary<string, HelpTopic> KeyTopics { get; }

        int Count { get; }

        Collection<HelpTopic> Topics { get; }

        HelpTopic GetKeyTopic(string key);

        HelpTopic SearchTopics(IEnumerable<string> words);

        Dictionary<HelpTopic, int> GetMatchRanks(IEnumerable<string> words);

        void LoadTopics(DirectoryInfo dir, bool appendTopics);
    }
}
