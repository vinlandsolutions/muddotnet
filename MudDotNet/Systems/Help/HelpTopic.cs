﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonCore;

namespace MudDotNet.Systems.Help
{
    public class HelpTopic
    {
        //public PragmaData Pragma { get; protected set; }

        public string Id { get; protected set; }

        public string Title { get; protected set; }

        public string Topic { get; protected set; }

        public IReadOnlyList<string> Keywords { get; protected set; }

        public StringBuilder Source { get; protected set; }

        public StringBuilder Content { get; protected set; }

        public int GetMatchValue(IEnumerable<string> words)
        {
            int result;
            int value = 0;
            result = GetMatchValue(this.Title, words);
            value += (result < 10 ? result : (int)(result * 1.00));
            result = GetMatchValue(this.Topic, words);
            value += (result < 10 ? result : (int)(result * 0.85));
            result = GetMatchValue(this.Keywords, words);
            value += (result < 20 ? (int)(result * 0.50) : (int)(result * 2.00));
            return value;
        }

        public void Load(FileInfo file)
        {
            if (this.Source == null) { this.Source = new StringBuilder(); }
            else if (this.Source.Length > 0) { this.Source.Clear(); }
            this.Source.AppendFile(file);
            this.Load();
        }

        public void Load(string source)
        {
            if (this.Source == null) { this.Source = new StringBuilder(); }
            else if (this.Source.Length > 0) { this.Source.Clear(); }
            this.Source.Append(source);
            this.Load();
        }

        public void Load(StringBuilder source)
        {
            if (this.Source == null) { this.Source = new StringBuilder(); }
            else if (this.Source.Length > 0) { this.Source.Clear(); }
            this.Source.Append(source);
            this.Load();
        }

        protected void Load()
        {
            if (this.Content == null) { this.Content = new StringBuilder(this.Source.Capacity, this.Source.MaxCapacity); }
            else if (this.Content.Length > 0) { this.Content.Clear(); }

            int count = PragmaParser.ParsePragmas(this.Source, out List<(string key, string value)> pragmas);
            foreach (var (key, value) in pragmas)
            {
                if (key == nameof(this.Id).ToLowerInvariant())
                {
                    this.Id = PragmaParser.ValidatePragma(this.Id, key, value, this.Content)
                        ? value : string.Empty;
                }
                else if (key == nameof(this.Title).ToLowerInvariant())
                {
                    this.Title = PragmaParser.ValidatePragma(this.Title, key, value, this.Content)
                        ? value : string.Empty;
                }
                else if (key == nameof(this.Topic).ToLowerInvariant())
                {
                    this.Topic = PragmaParser.ValidatePragma(this.Topic, key, value, this.Content)
                        ? value : string.Empty;
                }
                else if (key == nameof(this.Keywords).ToLowerInvariant())
                {
                    // Have to check the value length first so the validate pragma method
                    // doesn't log an empty keywords pragma as an error.
                    this.Keywords = value.Length == 0 || !PragmaParser.ValidatePragma(this.Keywords, key, value, this.Content)
                        ? Array.Empty<string>()
                        // Split the value by commas, trim each item and convert it to lower case,
                        // skip empty and duplicate items, and convert the sequence to an array.
                        : value.Split(',', StringSplitOptions.RemoveEmptyEntries)
                                .Select(item => item.Trim().ToLowerInvariant())
                                .Where(item => item.Length > 0)
                                .Distinct()
                                .ToArray();
                }
                else
                {
                    this.Content.AppendFormatLine("ERROR: Unknown pragma '{0}'", key);
                }
            }

            var missingMessage = "ERROR: Missing pragma '{0}'";
            if (this.Id == null) { this.Content.AppendFormatLine(missingMessage, nameof(this.Id).ToLowerInvariant()); }
            if (this.Title == null) { this.Content.AppendFormatLine(missingMessage, nameof(this.Title).ToLowerInvariant()); }
            if (this.Topic == null) { this.Content.AppendFormatLine(missingMessage, nameof(this.Topic).ToLowerInvariant()); }

            if (this.Content.IsNotEmpty()) { this.Content.AppendLine(); }
            foreach (var line in this.Source.ToLineSequence().Skip(count > 0 ? count + 1 : 0))
            { this.Content.AppendLine(line); }
        }





        public static int GetMatchValue(string source, string word)
        {
            int value = 0;
            if (source.IndexOf(word, StringComparison.InvariantCultureIgnoreCase) == -1) { return value; }
            else { value += 10; }

            if (source.Equals(word, StringComparison.InvariantCulture)) { value += 50; }
            else if (source.Equals(word, StringComparison.InvariantCultureIgnoreCase)) { value += 40; }
            else if (source.StartsWith(word + " ", StringComparison.InvariantCultureIgnoreCase)) { value += 35; }
            else if (source.StartsWith(word, StringComparison.InvariantCultureIgnoreCase)) { value += 30; }
            else if (source.EndsWith(" " + word, StringComparison.InvariantCultureIgnoreCase)) { value += 20; }
            else if (source.EndsWith(word, StringComparison.InvariantCultureIgnoreCase)) { value += 15; }
            else if (source.IndexOf(" " + word + " ", StringComparison.InvariantCultureIgnoreCase) > -1) { value += 20; }
            else if (source.IndexOf(" " + word, StringComparison.InvariantCultureIgnoreCase) > -1) { value += 15; }
            else if (source.IndexOf(word + " ", StringComparison.InvariantCultureIgnoreCase) > -1) { value += 15; }
            else { value -= 5; }
            return value;
        }

        public static int GetMatchValue(string source, IEnumerable<string> words)
        {
            int value = 0;
            foreach (var word in words) { value += GetMatchValue(source, word); }
            return value;
        }

        public static int GetMatchValue(IEnumerable<string> sources, IEnumerable<string> words)
        {
            int value = 0;
            foreach (var source in sources) { foreach (var word in words) { value += GetMatchValue(source, word); } }
            return value;
        }
    }
}
