﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace MudDotNet.Systems.Help
{
    public class HelpTopicManager : IHelpTopicManager
    {
        public Dictionary<string, HelpTopic> KeyTopics { get; } = new Dictionary<string, HelpTopic>();

        public Collection<HelpTopic> Topics { get; } = new Collection<HelpTopic>();

        public int Count => this.Topics.Count;

        public HelpTopic GetKeyTopic(string key) => (this.KeyTopics.ContainsKey(key) ? this.KeyTopics[key] : null);

        public HelpTopic SearchTopics(IEnumerable<string> words)
        {
            int highestValue = 0;
            HelpTopic highestTopic = null;
            foreach (var topic in this)
            {
                int value = topic.GetMatchValue(words);
                if (value > highestValue)
                {
                    highestValue = value;
                    highestTopic = topic;
                }
            }
            return highestTopic;
        }

        public Dictionary<HelpTopic, int> GetMatchRanks(IEnumerable<string> words)
        {
            var ranks = new Dictionary<HelpTopic, int>();
            foreach (var topic in this) { ranks[topic] = topic.GetMatchValue(words); }
            return ranks;
        }

        public void LoadTopics(DirectoryInfo dir, bool appendTopics)
        {
            if (!appendTopics)
            {
                this.Topics.Clear();
                this.KeyTopics.Clear();
            }
            foreach (var file in dir.EnumerateFiles("*.help", SearchOption.AllDirectories))
            {
                var topic = new HelpTopic();
                topic.Load(file);
                // maybe check some kind of validation on the topic before adding.
                // check if the list already contains this topic.
                this.Topics.Add(topic);
                if (!string.IsNullOrWhiteSpace(topic.Id) && !this.KeyTopics.ContainsKey(topic.Id))
                { this.KeyTopics.Add(topic.Id, topic); }
            }
        }

        public IEnumerator<HelpTopic> GetEnumerator() => this.Topics.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.Topics.GetEnumerator();
    }
}
