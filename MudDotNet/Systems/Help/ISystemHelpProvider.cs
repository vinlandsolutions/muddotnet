﻿namespace MudDotNet.Systems.Help
{
    public interface ISystemHelpProvider : ISystem
    {
        IHelpTopicManager Topics { get; }

        string Template { get; }
    }
}
