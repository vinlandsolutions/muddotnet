﻿using System.ComponentModel.Composition;
using System.IO;

namespace MudDotNet.Systems.Help
{
    [Export(typeof(ISystem))]
    [ExportMetadata("Title", "NMS Help System")]
    public class HelpSystem : ISystem, ISystemInitializable, ISystemFinalizeable, ISystemHelpProvider
    {
        public string SystemTitle { get; } = "NMS Help System";

        public IHelpTopicManager Topics { get; protected set; }

        public string Template { get; protected set; }

        public void SystemInitialize(ContextData context)
        {
            this.Topics = new HelpTopicManager();

            context.Logger.LogInc("Help Files Loading");
            this.Topics.LoadTopics(new DirectoryInfo(Path.Combine(".", "Data", "Help", "Content")), false);
            context.Logger.Log("Found '{0}' Help Files", this.Topics.Count);
            context.Logger.LogDec("Help Files Loaded");

            context.Logger.LogInc("Help Template Loading");
            this.Template = File.ReadAllText(Path.Combine(".", "Data", "Help", "Templates", "help.template"));
            context.Logger.LogDec("Help Template Loaded");
        }

        public void SystemFinalize(ContextData context)
        {
        }
    }
}
