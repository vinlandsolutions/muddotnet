﻿namespace MudDotNet.Systems
{
    public interface ISystemMetadata
    {
        string Title { get; }
    }
}
