﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MudDotNet.Systems
{
    public class SystemManager : ISystemManager
    {
        public SystemManager()
        {
            this._catalog = new AggregateCatalog();
            this._container = new CompositionContainer(this._catalog);
        }

        private readonly AggregateCatalog _catalog;

        private readonly CompositionContainer _container;

        [ImportMany]
        public List<Lazy<ISystem, ISystemMetadata>> Systems;

        public int Count => this.Systems.Count;

        public ISystem this[int index]
            => this.Systems.ElementAtOrDefault(index)?.Value;

        public void Compose()
            => this._container.ComposeParts(this);

        public ISystemManager Add(Assembly assembly)
        { this._catalog.Catalogs.Add(new AssemblyCatalog(assembly)); return this; }

        public ISystemManager Add(DirectoryInfo directory, string searchPattern = "*")
        { if (directory.Exists) { this._catalog.Catalogs.Add(new DirectoryCatalog(directory.FullName, searchPattern)); } return this; }

        public TSystem GetSystem<TSystem>() where TSystem : class, ISystem
        { foreach (var system in this) { if (system is TSystem) { return (TSystem)system; } } return null; }

        public IEnumerable<TSystem> GetSystems<TSystem>() where TSystem : class, ISystem
        { foreach (var system in this) { if (system is TSystem) { yield return (TSystem)system; } } }

        #region IEnumerable<ISystem> Support

        public IEnumerator<ISystem> GetEnumerator() { foreach (var lazy in this.Systems) { yield return lazy.Value; } }

        IEnumerator IEnumerable.GetEnumerator() { foreach (var lazy in this.Systems) { yield return lazy.Value; } }

        #endregion
    }
}
