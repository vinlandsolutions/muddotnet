﻿using System.Collections.Generic;
using System.IO;
using CommonCore.IO;
using Newtonsoft.Json;

namespace MudDotNet.Systems.Formatting
{
    public class TagInfos
    {
        public static TagInfos FromJsonFile(FileInfo file)
            => !file.Exists ? new TagInfos() : JsonConvert.DeserializeObject<TagInfos>(file.ReadAllText());

        public TagInfos() { }

        [JsonConstructor]
        public TagInfos(List<string[]> voids, List<string[]> blocks)
        {
            this.Voids = voids;
            this.Blocks = blocks;
        }

        public List<string[]> Voids { get; }

        public List<string[]> Blocks { get; }
    }
}
