﻿using System;

namespace MudDotNet.Systems.Formatting.Tags
{
    //public delegate string TagVoidHandlerDelegate(TagVoidHandlerData data);

    public readonly struct TagVoidData
    {
        public static TagVoidData Empty { get; } = new TagVoidData();

        public TagVoidData(string id, string tagReplacement = null, Func<TagVoidHandlerData, string> tagHandler = null)
        {
            this.Id = id;
            this.TagReplacement = tagReplacement;
            this.TagHandler = tagHandler;
        }

        public readonly string Id { get; }

        public readonly string TagReplacement { get; }

        public readonly bool TagReplaceable => !string.IsNullOrEmpty(this.TagReplacement);

        public readonly Func<TagVoidHandlerData, string> TagHandler { get; }

        public readonly bool TagHandleable => this.TagHandler != null;

        public override readonly int GetHashCode()
            => HashCode.Combine(this.Id, this.TagReplacement, this.TagHandler);

        public override readonly bool Equals(object obj)
            => obj is TagVoidData data ? this.Equals(data) : false;

        public readonly bool Equals(TagVoidData other)
            => Object.ReferenceEquals(this, other) ? true
            : this.Id == other.Id
                && this.TagReplacement == other.TagReplacement
                && this.TagHandler == other.TagHandler;

        public static bool operator ==(TagVoidData left, TagVoidData right) => left.Equals(right);

        public static bool operator !=(TagVoidData left, TagVoidData right) => !left.Equals(right);
    }
}
