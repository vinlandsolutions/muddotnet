﻿using System.Collections.Generic;

namespace MudDotNet.Systems.Formatting.Tags
{
    public class TagBlockStackManager
    {
        protected Dictionary<string, Stack<TagBlockData>> Stacks { get; } = new Dictionary<string, Stack<TagBlockData>>();

        public bool Has(string channel) => this.Stacks.ContainsKey(channel);

        public Stack<TagBlockData> Get(string channel) => (this.Has(channel) ? this.Stacks[channel] : null);

        public void Push(string channel, TagBlockData data)
        {
            if (!this.Has(channel)) { this.Stacks.Add(channel, new Stack<TagBlockData>()); }
            this.Stacks[channel].Push(data);
        }

        public TagBlockData Pop(string channel) => (this.Count(channel) > 0 ? this.Stacks[channel].Pop() : TagBlockData.Empty);

        public TagBlockData Peek(string channel) => (this.Count(channel) > 0 ? this.Stacks[channel].Peek() : TagBlockData.Empty);

        public int Count(string channel) => (this.Has(channel) ? this.Stacks[channel].Count : 0);
    }
}
