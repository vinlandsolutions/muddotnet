﻿using System;
using System.Collections.Generic;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Formatting.Tags
{
    public struct TagVoidHandlerData
    {
        public TagVoidHandlerData(IClient sender, IClient receiver, IList<IClient> list, string id, IList<string> args)
        {
            this.Sender = sender;
            this.Receiver = receiver;
            this.ClientList = list;
            this.TagId = id;
            this.TagArgs = args;
        }

        public IClient Sender { get; }

        public IClient Receiver { get; }

        public IList<IClient> ClientList { get; }

        public string TagId { get; }

        public IList<string> TagArgs { get; }
    }
}
