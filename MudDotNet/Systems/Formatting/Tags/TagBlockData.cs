﻿using System;

namespace MudDotNet.Systems.Formatting.Tags
{
    //public delegate string TagContentHandlerDelegate(string content, string[] args);

    public readonly struct TagBlockData
    {
        public static TagBlockData Empty { get; } = new TagBlockData();

        public TagBlockData(string id, string openReplacement = null, string closeReplacement = null,
            string stackChannel = null, Func<string, string[], string> contentHandler = null)
        {
            this.Id = id;
            this.OpenReplacement = openReplacement;
            this.CloseReplacement = closeReplacement;
            this.StackChannel = stackChannel;
            this.ContentHandler = contentHandler;
        }

        public readonly string Id { get; }

        public readonly string OpenReplacement { get; }

        public readonly string CloseReplacement { get; }

        public readonly string StackChannel { get; }

        public readonly Func<string, string[], string> ContentHandler { get; }

        public readonly bool OpenReplaceable => !string.IsNullOrEmpty(this.OpenReplacement);

        public readonly bool CloseReplaceable => !string.IsNullOrEmpty(this.CloseReplacement);

        public readonly bool Stackable => this.StackChannel != null;

        public readonly bool CanStack(TagBlockData other) => this.Stackable && other.Stackable && this.StackChannel == other.StackChannel;

        public override readonly int GetHashCode()
            => HashCode.Combine(this.Id, this.OpenReplacement, this.CloseReplacement, this.StackChannel, this.ContentHandler);
        public override readonly bool Equals(object obj)
            => obj is TagBlockData data ? this.Equals(data) : false;

        public readonly bool Equals(TagBlockData other)
            => Object.ReferenceEquals(this, other) ? true
            : this.Id == other.Id
                && this.OpenReplacement == other.OpenReplacement
                && this.CloseReplacement == other.CloseReplacement
                && this.ContentHandler == other.ContentHandler;

        public static bool operator ==(TagBlockData left, TagBlockData right) => left.Equals(right);

        public static bool operator !=(TagBlockData left, TagBlockData right) => !left.Equals(right);
    }
}
