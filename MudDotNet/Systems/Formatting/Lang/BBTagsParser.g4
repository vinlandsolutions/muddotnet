parser grammar BBTagsParser;

options { tokenVocab=BBTagsLexer; }

parse : (tagBlock | tagVoid | text | tagDangle)* ;

statement : (block=tagBlock | void=tagVoid | content=text) ;

text : TEXT ;

tagVoid : symbol=OPEN id=VALUE (COMMA args=VALUE)* SLASH CLOSE ;

tagBlock : open=tagOpen statement* close=tagClose ;

tagOpen : symbol=OPEN id=VALUE (COMMA VALUE)* CLOSE ;

tagClose : symbol=OPEN SLASH id=VALUE CLOSE ;

tagDangle : (tagOpen | tagClose) ;
