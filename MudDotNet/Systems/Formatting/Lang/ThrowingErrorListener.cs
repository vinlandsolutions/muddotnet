﻿using System;
using System.IO;
using Antlr4.Runtime;

namespace MudDotNet.Systems.Formatting.Lang.Core
{
    public class ThrowingErrorListener : BaseErrorListener
    {
        public static readonly ThrowingErrorListener Instance = new ThrowingErrorListener();

        public override void SyntaxError(TextWriter output, IRecognizer recognizer,
            IToken offendingSymbol, int line, int col, string msg, RecognitionException e)
        {
            throw new FormatException($"Syntax Error: {line}:{col} - {msg}");
        }
    }
}
