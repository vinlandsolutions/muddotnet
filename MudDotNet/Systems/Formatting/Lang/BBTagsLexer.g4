lexer grammar BBTagsLexer;

TEXT : (ESCAPE RB | ~'[')+ ;
OPEN : RB -> pushMode(MODE_CODE) ;

fragment RB : '[' ;
fragment ESCAPE : '\\' ;

mode MODE_CODE ;

CLOSE : (']') -> popMode ;
SLASH : '/' ;
COMMA : ',' ;
VALUE : (ESCSEQ | ~[,[\]/])* ;

WS : [ \t\r\n]+ -> skip ;

fragment ESCSEQ : ('\\,' | '\\[' | '\\]' | '\\/' | '\\\\') ;
