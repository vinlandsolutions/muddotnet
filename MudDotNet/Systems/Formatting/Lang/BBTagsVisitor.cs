﻿using System.Collections.Generic;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using MudDotNet.Networking;
using MudDotNet.Systems.Formatting.Lang.Core;
using MudDotNet.Systems.Formatting.Tags;
using static MudDotNet.Systems.Formatting.Lang.Core.BBTagsParser;

namespace MudDotNet.Systems.Formatting.Lang
{
    public class BBTagsVisitor : BBTagsParserBaseVisitor<string>
    {
        //public BBTagsVisitor(TagVoidManager voids, TagBlockManager blocks)
        //{
        //    this.TagVoidManager = voids;
        //    this.TagBlockManager = blocks;
        //}

        public BBTagsVisitor() { }

        public BBTagsVisitor(BBTagsManager manager)
        {
            this.Tags = manager;
        }

        public BBTagsManager Tags { get; set; }

        protected IClient Sender { get; set; }

        protected IClient Receiver { get; set; }

        protected IList<IClient> List { get; set; }

        public string Format(string input, IClient sender, IClient receiver, params IClient[] list)
        {
            if (string.IsNullOrEmpty(input)) { return input; }
            this.Sender = sender;
            this.Receiver = receiver;
            this.List = list;
            var parser = new BBTagsParser(new CommonTokenStream(new BBTagsLexer(new AntlrInputStream(input))));
            parser.RemoveErrorListeners();
            parser.AddErrorListener(ThrowingErrorListener.Instance);
            return this.Visit(parser.parse());
        }

        public override string VisitChildren(IRuleNode node)
        {
            string output = string.Empty;
            for (int index = 0; index < node.ChildCount; index++)
            {
                string result = this.Visit(node.GetChild(index));
                if (string.IsNullOrEmpty(result)) { continue; }
                output += result;
            }
            return output;
        }

        public override string VisitText([NotNull] TextContext context)
        {
            string text = context.GetText();
            text = text.Replace("\\[", "[");
            return text;
        }

        public override string VisitTagVoid([NotNull] TagVoidContext context)
        {
            string text = context.GetText();
            //if (context.symbol.Text == "[[") { return text.Substring(1); }
            string id = context.id.Text;
            //if (id == "lroll")
            //{ var x = 0; }

            string output = string.Empty;
            if (this.Tags.TagVoidDict != null && this.Tags.TagVoidDict.ContainsKey(id))
            {
                var data = this.Tags.TagVoidDict[id];
                if (data != TagVoidData.Empty)
                {
                    if (data.TagReplaceable) { output = data.TagReplacement; }
                    else if (data.TagHandleable)
                    {
                        var values = context.VALUE();
                        string[] args = BBTagsManager.ExtractArgs(values);
                        output = data.TagHandler(new TagVoidHandlerData(this.Sender, this.Receiver, this.List, id, args));
                    }
                }
            }
            else { output = text; }

            return output ?? string.Empty;
        }

        public override string VisitTagBlock([NotNull] TagBlockContext context)
        {
            string text = context.GetText();
            string idOpen = context.open.id.Text;
            string idClose = context.close.id.Text;

            //bool escaped = text[0] == '\\';

            bool echo = idOpen == "echo";
            bool skip = idOpen == "skip";

            TagBlockData data = TagBlockData.Empty;
            string open = string.Empty;
            string close = string.Empty;

            if (idOpen != idClose)
            {
                open = context.open.GetText();
                close = context.close.GetText();
            }
            else if (this.Tags.TagBlockDict != null && this.Tags.TagBlockDict.ContainsKey(idOpen))
            {
                data = this.Tags.TagBlockDict[idOpen];
                if (data != TagBlockData.Empty)
                {
                    if (data.OpenReplaceable)
                    {
                        open = data.OpenReplacement;
                        if (data.Stackable)
                        {
                            var prev = this.Tags.TagBlockStack.Peek(data.StackChannel);
                            if (data.CanStack(prev))
                            {
                                open = (prev.CloseReplacement ?? string.Empty) + open;
                            }
                        }
                    }
                    if (data.CloseReplaceable)
                    {
                        close = data.CloseReplacement;
                        if (data.Stackable)
                        {
                            var prev = this.Tags.TagBlockStack.Peek(data.StackChannel);
                            if (data.CanStack(prev))
                            {
                                close = close + (prev.OpenReplacement ?? string.Empty);
                            }
                        }
                    }
                }
            }

            if (data != TagBlockData.Empty && data.Stackable) { this.Tags.TagBlockStack.Push(data.StackChannel, data); }
            string content = this.VisitChildren(context);
            if (data != TagBlockData.Empty && data.Stackable) { this.Tags.TagBlockStack.Pop(data.StackChannel); }

            //string echoText = string.Empty;
            if (echo || skip || (data != TagBlockData.Empty && data.ContentHandler != null))
            {
                var values = context.open.VALUE();
                string[] args = BBTagsManager.ExtractArgs(values);
                if (echo)
                {
                    string echoText = string.Empty;
                    foreach (var element in context.statement()) { echoText += element.GetText(); }
                    return FormatHelper.Echo(content, echoText, args);
                }
                else if (skip)
                {
                    string skipText = string.Empty;
                    foreach (var element in context.statement()) { skipText += element.GetText(); }
                    return skipText;
                }
                else { content = data.ContentHandler(content, args); }
            }
            string output = string.Empty;
            //if (echo)
            //{
            //}
            //else { output = open + content + close; }
            output += open + content + close;
            return output;
        }

        public override string VisitTagDangle([NotNull] TagDangleContext context)
        {
            string text = context.GetText();
            return text;
        }

        //public override string VisitTagOpen([NotNull] TagOpenContext context)
        //{
        //    string text = context.GetText();
        //    //if (this.TagBlockHandlers == null) { return string.Empty; }
        //    //else if (context.symbol.Text == "[[") { return string.Empty; }

        //    //string id = context.id.Text;
        //    //if (!this.TagBlockHandlers.ContainsKey(id)) { return string.Empty; }

        //    //var data = this.TagBlockHandlers[id];
        //    //if (data == BlockData.Empty) { return string.Empty; }

        //    //this.TagBlockStack.Push(data);
        //    return string.Empty;
        //}

        //public override string VisitTagClose([NotNull] TagCloseContext context)
        //{
        //    string text = context.GetText();
        //    //if (this.TagBlockHandlers == null) { return string.Empty; }
        //    //else if (context.symbol.Text == "[[") { return string.Empty; }

        //    //string id = context.id.Text;
        //    //if (!this.TagBlockHandlers.ContainsKey(id)) { return string.Empty; }

        //    //var data = this.TagBlockHandlers[id];
        //    //if (data == BlockData.Empty) { return string.Empty; }

        //    //this.TagBlockStack.Pop();
        //    return string.Empty;
        //}
    }
}
