﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using CommonCore;
using CommonCore.IO;
using MudDotNet.Networking;
using MudDotNet.Systems.Formatting.Tags;

namespace MudDotNet.Systems.Formatting
{
    [Export(typeof(ISystem))]
    [ExportMetadata("Title", "NMS BBTags System")]
    public class BBTagsSystem : ISystem, ISystemInitializable, ISystemFinalizeable, ISystemOutputProcessor
    {
        public string SystemTitle { get; } = "NMS BBTags System";

        //public IServer Server { get; protected set; }

        public BBTagsManager TagsManager { get; protected set; }

        public Lang.BBTagsVisitor TagsVisitor { get; set; }

        public void SystemInitialize(ContextData context)
        {
            CSScriptLib.CSScript.Evaluator.ReferenceDomainAssemblies();
            this.TagsManager = new BBTagsManager();

            var tags = TagInfos.FromJsonFile(context.SystemInfo.DataDirectory.GetFile("TagInfos.json"));

            context.Logger.LogInc("Loading Void Tags");
            if (tags.Voids.IsNotEmpty())
            {
                int count = 0;
                context.Logger.Log("Replacement Tags");
                foreach (var tag in tags.Voids)
                {
                    // Skip unnamed and empty replacement elements.
                    if (tag[0] == null || tag[1] == null) { continue; }
                    this.TagsManager.AddVoid(tag[0], tag[1]);
                    count++;
                }
                context.Logger.Log("Found '{0}' Replacement Tags", count);

                count = 0;
                context.Logger.Log("Transform Tags");
                foreach (var tag in tags.Voids)
                {
                    // Skip unnamed, non-empty replacement, and empty transform elements.
                    if (tag[0] == null || tag[1] != null || tag[2] == null) { continue; }
                    var del = CreateDelegate<Func<TagVoidHandlerData, string>>(tag[2], "TagVoidHandlerData data",
                        "System", "System.Linq", "CommonCore",
                        "MudDotNet.Systems.Formatting", "MudDotNet.Systems.Formatting.Tags");
                    this.TagsManager.AddVoid(tag[0], del);
                    count++;
                }
                context.Logger.Log("Found '{0}' Transform Tags", count);
            }
            context.Logger.LogDec("Found '{0}' Void Tags", this.TagsManager.TagVoidDict.Count);

            context.Logger.LogInc("Loading Block Tags");
            if (tags.Blocks.IsNotEmpty())
            {
                int count = 0;
                context.Logger.Log("Replacement Tags");
                foreach (var tag in tags.Blocks)
                {
                    // Skip unnamed and empty replacement elements.
                    if (tag[0] == null || tag[1] == null) { continue; }
                    this.TagsManager.AddBlock(tag[0], tag[1], tag[2], tag[3]);
                    count++;
                }
                context.Logger.Log("Found '{0}' Replacement Tags", count);

                count = 0;
                context.Logger.Log("Transform Tags");
                foreach (var tag in tags.Blocks)
                {
                    // Skip unnamed, non-empty replacement, and empty transform elements.
                    if (tag[0] == null || tag[1] != null || tag[4] == null) { continue; }
                    var del = CreateDelegate<Func<string, string[], string>>(tag[4], "string content, string[] args",
                        "System", "System.Linq", "CommonCore",
                        "MudDotNet.Systems.Formatting");
                    this.TagsManager.AddBlock(tag[0], del);
                    count++;
                }
                context.Logger.Log("Found '{0}' Transform Tags", count);
            }
            context.Logger.LogDec("Found '{0}' Block Tags", this.TagsManager.TagBlockDict.Count);

            this.TagsVisitor = new Lang.BBTagsVisitor(this.TagsManager);
        }

        public void SystemFinalize(ContextData context)
        {
        }

        public bool HandlesOutput { get; } = true;

        public object CanProcessOutput(ContextData context, IClient sender, IClient receiver, StringBuilder output) => true;
        
        public bool ProcessOutput(ContextData context, IClient sender, IClient receiver, StringBuilder output, object data)
        {
            if (output.IsEmpty()) { return false; }

            string text = output.ToString();

            output.Clear();
            output.Append(this.TagsVisitor.Format(text, sender, receiver, null));
            
            return true;
        }









        public static T CreateDelegate<T>(string source, string argSig, params string[] namespaces) where T : class
        {
            string code = string.Empty;
            foreach (string @namespace in namespaces) { code += string.Format("using {0};\n", @namespace); }
            code += string.Format("public static string Transform({0})", argSig);
            code += source.IsChar(0, 'I') && source.IsNextChar(0, ':')
                ? string.Format(" => {0};", source[2..])
                : string.Format("\n{{\n{0}\n}}", source);
            return CompileDelegate<T>(code);
        }

        public static T CompileDelegate<T>(string code) where T: class
        {
            var asm = CSScriptLib.CSScript.Evaluator.CompileMethod(code);
            var method = asm.GetTypes().First(t => t.Name == "DynamicClass").GetMethods().First();
            return Delegate.CreateDelegate(typeof(T), method) as T;
        }













        public static void InitVoids(Dictionary<string, TagVoidData> voids)
        {
            ////voids.Add("securenext", new TagVoidData("securenext", "\x1B[4z"));

            //voids.Add("me", new TagVoidData("me", tagHandler: (data) => data.Sender == null ? "<NONE>" : data.Sender.Account.IsNone() ? "<GUEST>" : data.Sender.Account.Alias));

            //voids.Add("ime", new TagVoidData("ime", tagHandler: (data) =>
            //{
            //    if (data.Sender == null) { return "#-1"; }
            //    IPlayer player = data.Sender.GetPlayer();
            //    if (player == null) { return "#-1"; }
            //    return "#" + player.ID;
            //}));

            //voids.Add("ame", new TagVoidData("ame", tagHandler: (data) =>
            //{
            //    if (data.Sender == null) { return EAccessLevel.None.ToString(); }
            //    IPlayer player = data.Sender.GetPlayer();
            //    if (player == null) { return EAccessLevel.None.ToString(); }
            //    return player.Access.ToString();
            //}));

            //voids.Add("you", new TagVoidData("you", tagHandler: (data) =>
            //{
            //    if (data.Receiver == null) { return "<NONE>"; }
            //    //if (data.ClientList.Count == 0) { return "<NONE>"; }
            //    //var client = data.ClientList.ElementAt(0);
            //    //if (client == null) { return "<NONE>"; }
            //    IPlayer player = data.Receiver.GetPlayer();
            //    if (player == null) { return "<UNKNOWN>"; }
            //    return player.Name;
            //}));

            //voids.Add("iyou", new TagVoidData("iyou", tagHandler: (data) =>
            //{
            //    if (data.Receiver == null) { return "#-1"; }
            //    //if (data.ClientList.Count == 0) { return "#-1"; }
            //    //var client = data.ClientList.ElementAt(0);
            //    //if (client == null) { return "#-1"; }
            //    IPlayer player = data.Receiver.GetPlayer();
            //    if (player == null) { return "#-1"; }
            //    return "#" + player.ID;
            //}));

            //voids.Add("ayou", new TagVoidData("ayou", tagHandler: (data) =>
            //{
            //    if (data.Receiver == null) { return EAccessLevel.None.ToString(); }
            //    //if (data.ClientList.Count == 0) { return EAccessLevel.None.ToString(); }
            //    //var client = data.ClientList.ElementAt(0);
            //    //if (client == null) { return EAccessLevel.None.ToString(); }
            //    IPlayer player = data.Receiver.GetPlayer();
            //    if (player == null) { return EAccessLevel.None.ToString(); }
            //    return player.Access.ToString();
            //}));

            //voids.Add("here", new TagVoidData("here", tagHandler: (data) =>
            //{
            //    if (data.Sender == null) { return "<NONE>"; }
            //    IPlayer player = data.Sender.GetPlayer();
            //    if (player == null) { return "<NULL ROOM>"; }
            //    IRoom room = player.Manager.Rooms.Get(player.LocationID);
            //    if (room == null) { return "<NULL ROOM>"; }
            //    return room.Name;
            //}));

            //voids.Add("ihere", new TagVoidData("ihere", tagHandler: (data) =>
            //{
            //    if (data.Sender == null) { return "#-1"; }
            //    IPlayer player = data.Sender.GetPlayer();
            //    if (player == null) { return "#-1"; }
            //    IRoom room = player.Manager.Rooms.Get(player.LocationID);
            //    if (room == null) { return "#-1"; }
            //    return "#" + room.ID;
            //}));

            //voids.Add("there", new TagVoidData("there", tagHandler: (data) =>
            //{
            //    if (data.Receiver == null) { return "<NONE>"; }
            //    //if (data.ClientList.Count == 0) { return "<NONE>"; }
            //    //var client = data.ClientList.ElementAt(0);
            //    //if (client == null) { return "<NONE>"; }
            //    IPlayer player = data.Receiver.GetPlayer();
            //    if (player == null) { return "<NULL ROOM>"; }
            //    IRoom room = player.Manager.Rooms.Get(player.LocationID);
            //    if (room == null) { return "<NULL ROOM>"; }
            //    return room.Name;
            //}));

            //voids.Add("ithere", new TagVoidData("ithere", tagHandler: (data) =>
            //{
            //    if (data.Receiver == null) { return "#-1"; }
            //    //if (data.ClientList.Count == 0) { return "#-1"; }
            //    //var client = data.ClientList.ElementAt(0);
            //    //if (client == null) { return "#-1"; }
            //    IPlayer player = data.Receiver.GetPlayer();
            //    if (player == null) { return "#-1"; }
            //    IRoom room = player.Manager.Rooms.Get(player.LocationID);
            //    if (room == null) { return "#-1"; }
            //    return "#" + room.ID;
            //}));
        }
    }
}
