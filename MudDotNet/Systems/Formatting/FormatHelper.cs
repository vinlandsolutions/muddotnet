﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonCore;
using CommonCore.Experimental.Rand;

namespace MudDotNet.Systems.Formatting
{
    public static class FormatHelper
    {
        static FormatHelper()
        {
            FormatHelper.Rand = new Random(BitConverter.ToInt32(System.Guid.NewGuid().ToByteArray(), 0));
        }

        public static Random Rand { get; set; }

        public static T ElementAtOrDefault<T>(this IList<T> self, int index, T @default)
            => self.Count > index ? self[index] : @default;

        // Block Tag Functions

        public static string Echo(string content, string echoText, IList<string> args)
        {
            string format = args.ElementAtOrDefault(0, "{0}: {1}");
            return Echo(content, echoText, format);
        }

        public static string Echo(string content, string echoText, string format)
        {
            return string.Format(format, echoText, content);
        }

        public static string JustifyLeft(string content, IList<string> args)
        {
            int width = int.Parse(args.ElementAtOrDefault(0, "80"));
            string fill = args.ElementAtOrDefault(1, " ");
            return JustifyLeft(content, width, fill);
        }

        public static string JustifyLeft(string content, int width, string fill)
        {
            int length = content.Length;
            if (length >= width) { return content; }

            int space = width - length;
            int count = space / fill.Length;

            string text = string.Empty;
            for (int index = 0; index < count; index++) { text += fill; }
            if (text.Length < space) { text += fill.Substring(0, space - text.Length); }

            return content + text;
        }

        public static string JustifyRight(string content, IList<string> args)
        {
            int width = int.Parse(args.ElementAtOrDefault(0, "80"));
            string fill = args.ElementAtOrDefault(1, " ");
            return JustifyRight(content, width, fill);
        }

        public static string JustifyRight(string content, int width, string fill)
        {
            int length = content.Length;
            if (length >= width) { return content; }

            int space = width - length;
            int count = space / fill.Length;

            string text = string.Empty;
            for (int index = 0; index < count; index++) { text += fill; }
            if (text.Length < space) { text += fill.Substring(0, space - text.Length); }

            return text + content;
        }

        public static string JustifyCenter(string content, IList<string> args)
        {
            int width = int.Parse(args.ElementAtOrDefault(0, "80"));
            string leftFill = args.ElementAtOrDefault(1, " ");
            string rightFill = args.ElementAtOrDefault(2, new string(leftFill.ToCharArray().Reverse().ToArray()));
            return JustifyCenter(content, width, leftFill, rightFill);
        }

        public static string JustifyCenter(string content, int width, string leftFill, string rightFill)
        {
            var lines = content.Split('\n');
            content = string.Empty;
            foreach (var line in lines)
            {
                if (content != string.Empty) { content += '\n'; }
                if (line == string.Empty) { continue; }

                int length = line.Length;
                if (length >= width) { content += line; continue; }

                int leftSpace = (width / 2) - (length / 2);
                int rightSpace = width - length - leftSpace;
                int leftCount = leftSpace / leftFill.Length;
                int rightCount = rightSpace / rightFill.Length;

                string leftText = string.Empty;
                for (int index = 0; index < leftCount; index++) { leftText += leftFill; }
                if (leftText.Length < leftSpace) { leftText += leftFill.Substring(0, leftSpace - leftText.Length); }

                string rightText = string.Empty;
                for (int index = 0; index < rightCount; index++) { rightText += rightFill; }
                if (rightText.Length < rightSpace) { rightText += rightFill.Substring(0, rightSpace - rightText.Length); }

                content += leftText + line + rightText;
            }
            return content;
        }

        public static string CaseSentence(string content, IList<string> args)
        {
            return CaseSentence(content);
        }

        public static string CaseSentence(string content)
        {
            string output = char.ToUpper(content[0]).ToString();
            if (content.Length > 1) { output += content.Substring(1); }
            return output;
        }

        public static string CaseLower(string content, IList<string> args)
        {
            return CaseLower(content);
        }

        public static string CaseLower(string content)
        {
            return content.ToLower();
        }

        public static string CaseUpper(string content, IList<string> args)
        {
            return CaseUpper(content);
        }

        public static string CaseUpper(string content)
        {
            return content.ToUpper();
        }

        public static string CaseTitle(string content, IList<string> args)
        {
            string sep = args.ElementAtOrDefault(0, " ");
            return CaseTitle(content, sep);
        }

        public static string CaseTitle(string content, string sep)
        {
            var words = content.Split(new[] { sep }, StringSplitOptions.None);
            content = string.Empty;
            foreach (var word in words)
            {
                if (content != string.Empty) { content += sep; }
                content += CaseSentence(word);
            }
            return content;
        }

        public static string Concat(string content, IList<string> args)
        {
            string sepIn = args.ElementAtOrDefault(0, ",");
            string sepOut = args.ElementAtOrDefault(0, " ");
            return Concat(content, sepIn, sepOut);
        }

        public static string Concat(string content, string sepIn, string sepOut)
        {
            return string.Join(sepOut, content.Split(new[] { sepIn }, StringSplitOptions.None));
        }

        // Void Tag Functions

        //https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
        public static string NowDate(string id, IList<string> args)
        {
            string format = args.ElementAtOrDefault(0, "yyyy-MM-dd");
            return NowDate(id, format);
        }

        public static string NowDate(string id, string format)
        {
            return DateTime.UtcNow.ToString(format);
        }

        public static string NowTime(string id, IList<string> args)
        {
            string format = args.ElementAtOrDefault(0, "H:mm:ss");
            return NowTime(id, format);
        }

        public static string NowTime(string id, string format)
        {
            return DateTime.UtcNow.ToString(format);
        }

        public static string NowDateTime(string id, IList<string> args)
        {
            string format = args.ElementAtOrDefault(0, "yyyy-MM-dd H:mm:ss");
            return NowDateTime(id, format);
        }

        public static string NowDateTime(string id, string format)
        {
            return DateTime.UtcNow.ToString(format);
        }

        public static string Repeat(string id, IList<string> args)
        {
            char fill = args.ElementAtOrDefault(0, " ")[0];
            int count = int.Parse(args.ElementAtOrDefault(0, "80"));
            return Repeat(id, fill, count);
        }

        public static string Repeat(string id, char fill, int count)
        {
            return new string(fill, count);
        }

        public static string Roll(string id, IList<string> args)
        {
            int dice = GetNumber(args.ElementAtOrDefault(0, "1"));
            int sides = GetNumber(args.ElementAtOrDefault(1, "6"));
            return Roll(id, dice, sides);
        }

        public static string Roll(string id, int dice, int sides)
        {
            return RollDice(dice, sides).ToString();
        }

        public static string RollList(string id, IList<string> args)
        {
            int dice = GetNumber(args.ElementAtOrDefault(0, "1"));
            int sides = GetNumber(args.ElementAtOrDefault(1, "6"));
            string sep = args.ElementAtOrDefault(2, " ");
            string format = args.ElementAtOrDefault(3, "{0}d{1}: [{2}] = {3}");
            return RollList(id, dice, sides, sep, format);
        }

        public static string RollList(string id, int dice, int sides, string sep, string format)
        {
            int total = 0;
            string output = string.Empty;
            for (int index = 0; index < dice; index++)
            {
                if (output != string.Empty) { output += sep; }
                int roll = RollDice(1, sides);
                total += roll;
                output += roll;
            }
            return string.Format(format, dice, sides, output, total);
        }

        public static int GetNumber(string text)
        {
            string number = text.RemoveAll(",", "_", " ");
            return int.Parse(number);
        }

        public static int RollDice(int dice, int sides)
        {
            if (dice == 0 || sides == 0) { return 0; }

            bool negDice = dice < 0, negSides = sides < 0;
            bool neg = (negDice && !negSides) || (!negDice && negSides);
            if (negDice) { dice *= -1; }
            if (negSides) { sides *= -1; }

            int result = Rand.GetInt(dice, (dice * sides) + 1);
            if (neg) { result *= -1; }

            return result;
        }
    }
}
