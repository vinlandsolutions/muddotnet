﻿using System;
using System.Collections.Generic;
using Antlr4.Runtime.Tree;
using CommonCore;
using MudDotNet.Systems.Formatting.Tags;

namespace MudDotNet.Systems.Formatting
{
    public class BBTagsManager
    {
        public BBTagsManager() { }

        public Dictionary<string, TagVoidData> TagVoidDict { get; } = new Dictionary<string, TagVoidData>();

        public Dictionary<string, TagBlockData> TagBlockDict { get; } = new Dictionary<string, TagBlockData>();

        public TagBlockStackManager TagBlockStack { get; } = new TagBlockStackManager();

        public void AddVoid(string id, string replacement)
            => this.TagVoidDict.Add(id, new TagVoidData(id, replacement));

        public void AddVoid(string id, Func<TagVoidHandlerData, string> tagHandler)
            => this.TagVoidDict.Add(id, new TagVoidData(id, tagHandler: tagHandler));

        public void AddBlock(string id, string openReplacement, string closeReplacement, string stackChannel)
            => this.TagBlockDict.Add(id, new TagBlockData(id, openReplacement, closeReplacement, stackChannel));

        public void AddBlock(string id, Func<string, string[], string> contentHandler)
            => this.TagBlockDict.Add(id, new TagBlockData(id, contentHandler: contentHandler));

        public static string[] ExtractArgs(ITerminalNode[] values)
        {
            if (values == null || values.Length < 2) { return Array.Empty<string>(); }

            var args = new string[values.Length - 1];
            for (int index = 0; index < args.Length; index++)
            {
                string arg = values[index + 1].GetText();
                arg = arg.ReplaceAny(new[] { "\\,", "\\[", "\\]", "\\/", "\\\\", "\\n" },
                                    new[] { ",", "[", "]", "/", "\\", "\n" });
                args[index] = arg;
            }
            return args;
        }
    }
}
