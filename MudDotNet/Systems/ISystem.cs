﻿using System.Text;
using MudDotNet.Networking;

namespace MudDotNet.Systems
{
    public interface ISystem
    {
        string SystemTitle { get; }
    }

    public interface ISystemInitializable : ISystem
    {
        void SystemInitialize(ContextData context);
    }

    public interface ISystemFinalizeable : ISystem
    {
        void SystemFinalize(ContextData context);
    }

    public interface ISystemInputProcessor : ISystem
    {
        bool HandlesInput { get; }

        object CanProcessInput(ContextData context, IClient client, StringBuilder input);

        StringBuilder ProcessInput(ContextData context, IClient client, StringBuilder input, object data);
    }

    public interface ISystemOutputProcessor : ISystem
    {
        bool HandlesOutput { get; }

        object CanProcessOutput(ContextData context, IClient sender, IClient receiver, StringBuilder output);

        bool ProcessOutput(ContextData context, IClient sender, IClient receiver, StringBuilder output, object data);
    }
}
