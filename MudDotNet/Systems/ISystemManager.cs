﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MudDotNet.Systems
{
    public interface ISystemManager : IEnumerable<ISystem>
    {
        int Count { get; }

        void Compose();

        ISystemManager Add(Assembly assembly);

        ISystemManager Add(DirectoryInfo directory, string searchPattern);

        ISystem this[int index] { get; }

        TSystem GetSystem<TSystem>() where TSystem : class, ISystem;

        IEnumerable<TSystem> GetSystems<TSystem>() where TSystem : class, ISystem;

    }
}
