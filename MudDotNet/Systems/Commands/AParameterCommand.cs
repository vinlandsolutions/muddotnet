﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using CommonCore;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands
//{
//    public abstract class AParameterCommand : ACommand
//    {
//        protected AParameterCommand(string name, EAccessLevel access)
//            : this(name, access, 0, 0, null) { }

//        protected AParameterCommand(string name, EAccessLevel access, int argCount)
//            : this(name, access, argCount, argCount, null) { }

//        protected AParameterCommand(string name, EAccessLevel access, int argCount, string[] validSwitches)
//            : this(name, access, argCount, argCount, validSwitches) { }

//        protected AParameterCommand(string name, EAccessLevel access, int argCountMin, int argCountMax)
//            : this(name, access, argCountMin, argCountMax, null) { }

//        //protected AParameterCommand(string name, EAccessLevel access, string[] validSwitches)
//        //    : this(name, access, 0, 0, validSwitches) { }

//        protected AParameterCommand(string name, EAccessLevel access, int argCountMin, int argCountMax, string[] validSwitches)
//            : base(name, access)
//        {
//            this.ArgCountMin = argCountMin;
//            this.ArgCountMax = argCountMax;
//            this.ValidSwitches = validSwitches;
//        }

//        //public virtual int ArgCountMin { get; protected set; } = 0;

//        //public virtual int ArgCountMax { get; protected set; } = 0;

//        //public virtual IReadOnlyList<string> ValidSwitches { get; protected set; }

//        public override StringBuilder ProcessInput(ContextData context, IClient client, StringBuilder input)
//        {
//            this.ParseCommand(input, out string key, out IList<string> switches, out IList<string> args);

//            if (!this.MatchKey(context, client, key)) { return null; }

//            var result = new ValidateResult();
//            if (!this.ValidateAccess(context, client, ref result)) { return new StringBuilder(result.Message); }
//            if (!this.ValidateSwitches(context, client, switches, ref result)) { return new StringBuilder(result.Message); }
//            if (!this.ValidateArgCount(context, client, args, ref result)) { return new StringBuilder(result.Message); }

//            var output = new StringBuilder();
//            this.ProcessInput(context, client, key, switches, args, output);
//            return output;
//        }

//        public abstract void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output);

//        protected virtual void ParseCommand(StringBuilder input, out string key, out IList<string> switches, out IList<string> args)
//            => CommandHelper.ParseCommand(input, out key, out switches, out args);

//        protected virtual bool MatchKey(ContextData context, IClient client, string key)
//            => this.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase);

//        protected virtual bool ValidateSwitches(ContextData context, IClient client, IList<string> switches, ref ValidateResult result)
//        {
//            if (this.ValidSwitches.IsEmpty() && switches.IsEmpty()) { return true; }
//            else if (this.ValidSwitches.IsEmpty() && !switches.IsEmpty()) { result.Message = $"ERROR: The '{this.Name}' command does not accept switches."; }

//            if (result.HasMessage) { return false; }

//            var invalid = switches.Except(this.ValidSwitches);
//            if (!invalid.Any()) { result.Message = $"ERROR: The '{this.Name}' command does not accept '{string.Join(", ", invalid)}' switches."; }

//            return !result.HasMessage;
//        }

//        protected virtual bool ValidateArgCount(ContextData context, IClient client, IList<string> args, ref ValidateResult result)
//        {
//            if (this.ArgCountMax < 0 && this.ArgCountMin <= 0) { return true; }
//            else if (this.ArgCountMax == 0 && args.IsEmpty()) { return true; }
//            else if (this.ArgCountMax == 0 && !args.IsEmpty()) { result.Message = $"ERROR: The '{this.Name}' command does not accept arguments."; }
//            else if (this.ArgCountMax > 0 && args.Count > this.ArgCountMax) { result.Message = $"ERROR: The '{this.Name}' command accepts maximum {this.ArgCountMax} arguments, not {args.Count}."; }
//            else if (this.ArgCountMin > 0 && args.Count < this.ArgCountMin) { result.Message = $"ERROR: The '{this.Name}' command requires minimum {this.ArgCountMin} arguments, not {args.Count}."; }

//            return !result.HasMessage;
//        }
//    }
//}
