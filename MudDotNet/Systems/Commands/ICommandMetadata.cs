﻿namespace MudDotNet.Systems.Commands
{
    public interface ICommandMetadata
    {
        string Title { get; }
    }
}
