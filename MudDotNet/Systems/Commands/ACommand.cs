﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands
{
    public abstract class ACommand : ICommand
    {
        protected ACommand(string name, EAccessLevel access)
            : this(name, access, 0, 0, null) { }

        protected ACommand(string name, EAccessLevel access, int argCount)
            : this(name, access, argCount, argCount, null) { }

        protected ACommand(string name, EAccessLevel access, int argCount, string[] validSwitches)
            : this(name, access, argCount, argCount, validSwitches) { }

        protected ACommand(string name, EAccessLevel access, int argCountMin, int argCountMax)
            : this(name, access, argCountMin, argCountMax, null) { }

        //protected ACommand(string name, EAccessLevel access, string[] validSwitches)
        //    : this(name, access, 0, 0, validSwitches) { }

        protected ACommand(string name, EAccessLevel access, int argCountMin, int argCountMax, string[]? validSwitches)
        {
            this.Name = name;
            this.Access = access;
            this.ArgCountMin = argCountMin;
            this.ArgCountMax = argCountMax;
            this.ValidSwitches = validSwitches ?? Array.Empty<string>();
        }

        public virtual string Name { get; protected set; }

        public virtual EAccessLevel Access { get; protected set; }

        public virtual int ArgCountMin { get; protected set; } = 0;

        public virtual int ArgCountMax { get; protected set; } = 0;

        public virtual IReadOnlyList<string> ValidSwitches { get; protected set; }

        public virtual object? Initialize(ContextData context)
        {
            //this.Server = server;
            return null;
        }

        public virtual StringBuilder ProcessInput(ContextData context, IClient client, StringBuilder input)
        {
            Throw.If.Arg.Null(nameof(client), client);
            Throw.If.Arg.Null(nameof(input), input);

            this.ParseCommand(input, out string key, out IList<string> switches, out IList<string> args);

            if (!this.MatchKey(context, client, key)) { return null; }

            var result = new ValidateResult();
            if (!this.ValidateAccess(context, client, ref result)) { return new StringBuilder(result.Message); }
            if (!this.ValidateSwitches(context, client, switches, ref result)) { return new StringBuilder(result.Message); }
            if (!this.ValidateArgCount(context, client, args, ref result)) { return new StringBuilder(result.Message); }

            var output = new StringBuilder();
            this.ProcessInput(context, client, key, switches, args, output);
            return output;
        }

        public virtual void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
        {
            Throw.If.Arg.Null(nameof(client), client);
            Throw.If.Arg.Null(nameof(key), key);
            Throw.If.Arg.Null(nameof(switches), switches);
            Throw.If.Arg.Null(nameof(args), args);
            Throw.If.Arg.Null(nameof(output), output);
        }

        protected virtual void ParseCommand(StringBuilder input, out string key, out IList<string> switches, out IList<string> args)
        {
            Throw.If.Arg.Null(nameof(input), input);
            CommandHelper.ParseCommand(input, out key, out switches, out args);
        }

        protected virtual bool MatchKey(ContextData context, IClient client, string key)
            => this.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase);

        protected virtual bool ValidateAccess(ContextData context, IClient client, ref ValidateResult result)
        {
            if (this.Access == EAccessLevel.Any) { return true; }
            //else if (client.EntityID < 0) { result.Message = "ERROR: You are not connected to the game."; }
            //else if (client.Access < this.Access) { result.Message = "ERROR: You do not have access to this command."; }

            return !result.HasMessage;
        }

        protected virtual bool ValidateSwitches(ContextData context, IClient client, IList<string> switches, ref ValidateResult result)
        {
            if (this.ValidSwitches.IsEmpty() && switches.IsEmpty()) { return true; }
            else if (this.ValidSwitches.IsEmpty() && !switches.IsEmpty()) { result.Message = $"ERROR: The '{this.Name}' command does not accept switches."; }

            if (result.HasMessage) { return false; }

            var invalid = switches.Except(this.ValidSwitches);
            if (!invalid.Any()) { result.Message = $"ERROR: The '{this.Name}' command does not accept '{string.Join(", ", invalid)}' switches."; }

            return !result.HasMessage;
        }

        protected virtual bool ValidateArgCount(ContextData context, IClient client, IList<string> args, ref ValidateResult result)
        {
            if (this.ArgCountMax < 0 && this.ArgCountMin <= 0) { return true; }
            else if (this.ArgCountMax == 0 && args.IsEmpty()) { return true; }
            else if (this.ArgCountMax == 0 && !args.IsEmpty()) { result.Message = $"ERROR: The '{this.Name}' command does not accept arguments."; }
            else if (this.ArgCountMax > 0 && args.Count > this.ArgCountMax) { result.Message = $"ERROR: The '{this.Name}' command accepts maximum {this.ArgCountMax} arguments, not {args.Count}."; }
            else if (this.ArgCountMin > 0 && args.Count < this.ArgCountMin) { result.Message = $"ERROR: The '{this.Name}' command requires minimum {this.ArgCountMin} arguments, not {args.Count}."; }

            return !result.HasMessage;
        }
    }
}
