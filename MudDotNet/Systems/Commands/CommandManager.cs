﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MudDotNet.Systems.Commands
{
    public class CommandManager : ICommandManager
    {
        public CommandManager()
        {
            this._catalog = new AggregateCatalog();
            this._container = new CompositionContainer(this._catalog);
        }

        private readonly AggregateCatalog _catalog;

        private readonly CompositionContainer _container;

        [ImportMany]
        public List<Lazy<ICommand, ICommandMetadata>> Commands;

        public int Count => this.Commands.Count;

        public ICommand this[int index] => this.Commands.ElementAtOrDefault(index)?.Value;

        public ICommand this[string name]
        {
            get
            {
                foreach (var command in this)
                {
                    if (command.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return command;
                    }
                }
                return null;
            }
        }

        public void Compose()
            => this._container.ComposeParts(this);

        public void Add(Assembly assembly)
            => this._catalog.Catalogs.Add(new AssemblyCatalog(assembly));

        public void Add(string directory, string searchPattern = "*")
            => this._catalog.Catalogs.Add(new DirectoryCatalog(directory, searchPattern));

        public void Add(DirectoryInfo directory, string searchPattern = "*")
            => this._catalog.Catalogs.Add(new DirectoryCatalog(directory.FullName, searchPattern));

        public IEnumerator<ICommand> GetEnumerator() { foreach (var lazy in this.Commands) { yield return lazy.Value; } }

        IEnumerator IEnumerable.GetEnumerator() { foreach (var lazy in this.Commands) { yield return lazy.Value; } }
    }
}
