﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace MudDotNet.Systems.Commands
{
    public interface ICommandManager : IEnumerable<ICommand>
    {
        int Count { get; }

        ICommand this[int index] { get; }

        ICommand this[string name] { get; }

        void Compose();

        void Add(Assembly assembly);

        void Add(string directory, string searchPattern);

        void Add(DirectoryInfo directory, string searchPattern);
    }
}
