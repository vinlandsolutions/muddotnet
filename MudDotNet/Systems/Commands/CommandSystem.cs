﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands
{
    [Export(typeof(ISystem))]
    [ExportMetadata("Title", "NMS Command System")]
    public class CommandSystem : ISystem, ISystemInitializable, ISystemFinalizeable, ISystemInputProcessor
    {
        public string SystemTitle { get; } = "NMS Command System";

        //public IServer Server { get; protected set; }

        public ICommandManager CommandManager { get; protected set; }

        public void SystemInitialize(ContextData context)
        {
            //this.Server = server;
            this.CommandManager = new CommandManager();
            context.Logger.Log("Loading Commands");
            this.CommandManager.Add(typeof(Terminal).Assembly);
            this.CommandManager.Add("./Commands", "*.Commands.*.dll");
            this.CommandManager.Add(context.SystemInfo.SystemsDirectory, "*.Systems.*.dll");
            context.Logger.Log("Composing Commands");
            this.CommandManager.Compose();
            context.Logger.Log("Found '{0}' Commands", this.CommandManager.Count);

            if (this.CommandManager.Count > 0)
            {
                foreach (var command in this.CommandManager)
                {
                    context.Logger.LogInc("'{0}' Initializing", command.Name);
                    command.Initialize(context);
                    context.Logger.LogDec("'{0}' Initialized", command.Name);
                }
            }
        }

        public void SystemFinalize(ContextData context)
        {
            foreach (ICommand command in this.CommandManager)
            {
                // maybe have commands be finalizeable.
            }
        }

        public bool HandlesInput { get; } = true;

        public object CanProcessInput(ContextData context, IClient client, StringBuilder input) => true;

        private readonly char[] _sepsArgs = new char[] { ' ' };
        private readonly char[] _sepsSwitches = new char[] { '/' };
        public StringBuilder ProcessInput(ContextData context, IClient client, StringBuilder input, object data)
        {
            if (input.IsEmpty()) { return null; }
            
            foreach (var command in this.CommandManager)
            {
                var output = command.ProcessInput(context, client, input);
                if (output == null) { continue; }
                return output;
            }
            return null;
        }
    }
}
