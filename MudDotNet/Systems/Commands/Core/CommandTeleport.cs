﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Text;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "Teleport")]
//    public class CommandTeleport : ACoreCommand
//    {
//        public CommandTeleport() : base("teleport", EAccessLevel.Member, 1, 2)
//        {
//        }

//        protected override bool MatchKey(IServer server, IClient client, string key)
//        {
//            return key.StartsWith("tel");
//        }

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            DateTime now = DateTime.UtcNow;
//            IPlayer player = client.GetPlayer();
            
//            IPlayer targetPlayer;
//            IRoom destRoom;
//            if (args.Count == 1)
//            {
//                targetPlayer = player;
//                long dest = EntityHelper.ParseID(client, args[0]);
//                destRoom = this.EntitySystem.Manager.Rooms.Get(dest);
//                if (dest > -1 && destRoom == null)
//                {
//                    output.Append($"ERROR: The first argument '{args[0]}' is not a valid room id.");
//                    return;
//                }
//            }
//            else
//            {
//                long target = EntityHelper.ParseID(client, args[0]);
//                targetPlayer = this.EntitySystem.Manager.Players.Get(target);
//                if (targetPlayer == null)
//                {
//                    output.Append($"ERROR: The first argument '{args[0]}' is not a valid player id.");
//                    return;
//                }
//                long dest = EntityHelper.ParseID(client, args[1]);
//                destRoom = this.EntitySystem.Manager.Rooms.Get(dest);
//                if (dest > -1 && destRoom == null)
//                {
//                    output.Append($"ERROR: The second argument '{args[1]}' is not a valid room id.");
//                    return;
//                }
//            }

//            if (player.ID != targetPlayer.ID && player.Access <= targetPlayer.Access)
//            {
//                output.Append($"ERROR: Your permission level '{player.Access}' can not teleport greater or equal permission levels ({targetPlayer.Access}).");
//                return;
//            }

//            IRoom sourceRoom = this.EntitySystem.Manager.Rooms.Get(targetPlayer.LocationID);
            
//            sourceRoom?.RemovePlayer(targetPlayer, null);
//            destRoom?.AddPlayer(targetPlayer, null);
//        }
//    }
//}
