﻿//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Text;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core.Pueblo
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "PuebloClient")]
//    public class CommandPuebloClient : ACoreCommand
//    {
//        public CommandPuebloClient() : base("puebloclient", EAccessLevel.Any, 2, -1)
//        {
//            /* First, send the below text, either at connection or after logging in:
//             * "This world is Pueblo 2.50 enhanced."
//             * When the client receives the above line, it will send back:
//             * "PUEBLOCLIENT 2.50 md5=\"checksum string\""
//             * It may have more arguments, but the checksum is the important one. Save both the args for late use.
//             * Anyway, after we receive the above from the client, we then send the following to turn on Pueblo:
//             * "</xch_mudtext><img xch_mode=html>" or "</xch_mudtext><img xch_mode=purehtml>"
//             * See the following link for differences between html and purehtml: http://pueblo.sourceforge.net/doc/manual/html_xch_mode.html
//            */
//        }

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            var data = new PuebloData(args[0], args[1].Substring(5).TrimEnd('"'));
//            client.Pueblo = data;
//            server.Broadcast(null, client, new StringBuilder("</xch_mudtext><img xch_mode=html>"));
//            //server.Broadcast(null, client, new StringBuilder("<a xch_cmd=\"look here\" xch_hint=\"Look Here\">TesT</a>"));
//        }
//    }
//}
