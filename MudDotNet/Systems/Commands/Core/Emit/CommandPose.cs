﻿using System.ComponentModel.Composition;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Emit
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Pose")]
    public class CommandPose : AEmitCommand
    {
        public CommandPose() : base("pose", ":", EAccessLevel.Member)
        {
        }

        protected override void ProcessInput(ContextData context, IClient client, string key, StringBuilder input, StringBuilder output)
        {
            if (input.IsEmpty()) { output.Append("You pose nothing."); }
            else { this.BroadcastLocal(context, client, "[me/] {0}", input); }
        }
    }
}
