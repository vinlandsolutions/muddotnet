﻿using System.ComponentModel.Composition;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Emit
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Say")]
    public class CommandSay : AEmitCommand
    {
        public CommandSay() : base("say", "\"", EAccessLevel.Member) { }

        protected override void ProcessInput(ContextData context, IClient client, string key, StringBuilder input, StringBuilder output)
        {
            if (input.IsEmpty()) { output.Append("You say nothing."); }
            else { this.BroadcastLocal(context, client, "You say, \"{0}\"", "[me/] says, \"{0}\"", input); }
        }
    }
}
