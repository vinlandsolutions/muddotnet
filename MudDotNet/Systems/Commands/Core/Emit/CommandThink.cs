﻿using System.ComponentModel.Composition;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Emit
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Think")]
    public class CommandThink : AEmitCommand
    {
        public CommandThink() : base("think", ".", EAccessLevel.Any) { }

        protected override void ProcessInput(ContextData context, IClient client, string key, StringBuilder input, StringBuilder output)
        {
            if (input.IsEmpty()) { output.Append("You think nothing."); }
            else { this.Broadcast(context, client, client, input.Clone()); }
        }
    }
}
