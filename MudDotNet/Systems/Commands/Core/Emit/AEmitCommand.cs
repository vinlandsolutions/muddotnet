﻿using System;
using System.Collections.Generic;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Emit
{
    public abstract class AEmitCommand : ACommand
    {
        protected AEmitCommand(string name, string symbol, EAccessLevel access) : base(name, access)
        {
            this.Symbol = symbol;
        }

        //protected EntitySystem EntitySystem { get; set; }

        public override object Initialize(ContextData context)
        {
            var result = base.Initialize(context);
            //this.EntitySystem = this.Server.Systems.GetSystem<EntitySystem>();
            return result;
        }

        public virtual string Symbol { get; protected set; }

        public override StringBuilder ProcessInput(ContextData context, IClient client, StringBuilder input)
        {
            string key = this.Symbol;
            if (!this.ParseKey(context, client, input, ref key)) { return null; }

            var result = new ValidateResult();
            if (!this.ValidateAccess(context, client, ref result)) { return new StringBuilder(result.Message); }

            var output = new StringBuilder();
            this.ProcessInput(context, client, key, input, output);
            return output;
        }

        protected abstract void ProcessInput(ContextData context, IClient client, string key, StringBuilder input, StringBuilder output);

        protected virtual bool ParseKey(ContextData context, IClient client, StringBuilder input, ref string key)
        {
            if (input.StartsWith(this.Symbol, ECharComparison.InvariantCultureIgnoreCase))
            {
                key = this.Symbol;
                input.Remove(0, key.Length);
                return true;
            }
            else if (input.StartsWith(this.Name + " ", ECharComparison.InvariantCultureIgnoreCase))
            {
                key = this.Name;
                input.Remove(0, key.Length + 1);
                return true;
            }
            return false;
        }

        //protected virtual void BroadcastLocal(ContextData context, IClient sender, StringBuilder output)
        //{
        //    var player = this.EntitySystem.Manager.Players.Get(sender.EntityID);
        //    foreach (IClient other in this.Server.Clients)
        //    {
        //        var otherPlayer = this.EntitySystem.Manager.Players.Get(other.EntityID);
        //        if (otherPlayer == null) { continue; }
        //        if (player.LocationID != otherPlayer.LocationID) { continue; }

        //        this.Server.Broadcast(sender, other, output.Clone());
        //    }
        //}

        //protected virtual void BroadcastLocal(ContextData context, IClient sender, string format, StringBuilder output)
        //{
        //    var player = this.EntitySystem.Manager.Players.Get(sender.EntityID);
        //    foreach (IClient other in this.Server.Clients)
        //    {
        //        var otherPlayer = this.EntitySystem.Manager.Players.Get(other.EntityID);
        //        if (otherPlayer == null) { continue; }
        //        if (player.LocationID != otherPlayer.LocationID) { continue; }

        //        this.Server.Broadcast(sender, other, new StringBuilder(string.Format(format, output.ToString())));
        //    }
        //}

        protected virtual void BroadcastLocal(ContextData context, IClient sender, string format, StringBuilder output)
            => this.BroadcastLocal(context, sender,
                new StringBuilder(string.Format(format, output.ToString())),
                new StringBuilder(string.Format(format, output.ToString())));

        protected virtual void BroadcastLocal(ContextData context, IClient sender, string formatMe, string formatYou, StringBuilder output)
            => this.BroadcastLocal(context, sender,
                new StringBuilder(string.Format(formatMe, output.ToString())),
                new StringBuilder(string.Format(formatYou, output.ToString())));

        protected virtual void BroadcastLocal(ContextData context, IClient sender, StringBuilder textMe, StringBuilder textYou)
        {
            foreach (var client in context.Clients)
            {
                context.Server.Broadcast(sender, client, sender == client ? textMe : textYou);
            }
            //var player = this.EntitySystem.Manager.Players.Get(sender.EntityID);
            //foreach (IClient other in context.Clients)
            //{
            //    var otherPlayer = this.EntitySystem.Manager.Players.Get(other.EntityID);
            //    if (otherPlayer == null) { continue; }
            //    if (player.LocationID != otherPlayer.LocationID) { continue; }

            //    StringBuilder text;
            //    if (sender == other) { text = new StringBuilder(string.Format(formatMe, output.ToString())); }
            //    else { text = new StringBuilder(string.Format(formatYou, output.ToString())); }
            //    this.Server.Broadcast(sender, other, text);
            //}
        }

        protected virtual void Broadcast(ContextData context, IClient sender, IEnumerable<IClient> receivers, StringBuilder output)
        {
            foreach (var receiver in receivers)
            {
                context.Server.Broadcast(sender, receiver, output.Clone());
            }
        }

        protected virtual void Broadcast(ContextData context, IClient sender, IClient receiver, StringBuilder output)
        {
            context.Server.Broadcast(sender, receiver, output);
        }
    }
}
