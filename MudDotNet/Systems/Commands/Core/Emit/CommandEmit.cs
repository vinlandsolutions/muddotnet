﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using CommonCore;
using MudDotNet.Networking;
using System.Linq;
using System;

namespace MudDotNet.Systems.Commands.Core.Emit
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Emit")]
    public class CommandEmit : AEmitCommand
    {
        public CommandEmit() : base("emit", "\\", EAccessLevel.Member)
        {
        }

        protected override void ProcessInput(ContextData context, IClient client, string key, StringBuilder input, StringBuilder output)
        {
            if (input.IsEmpty()) { output.Append("You emit nothing."); }
            else { this.BroadcastLocal(context, client, input, input); }
        }
    }
}


