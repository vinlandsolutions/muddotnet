﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Text;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "Set")]
//    public class CommandSet : ACoreCommand
//    {
//        public CommandSet() : base("set", EAccessLevel.Member, 1, 2)
//        {
//        }

//        protected override void ParseCommand(StringBuilder input, ref string key, ref IList<string> switches, ref IList<string> args)
//        {
//            CommandHelper.ParseCommand(input, ref key, ref switches, ref args, this.ArgCountMax, Util.EParseRemainderAction.Add);
//        }

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            DateTime now = DateTime.UtcNow;
//            IPlayer clientPlayer = client.GetPlayer();

//            string target = args[0];
//            string value = (args.Count == 1 ? null : args[1]);

//            if (target == "desc")
//            {
//                clientPlayer.Description = value;
//                output.AppendLine($"{clientPlayer.Name} '{target}' set to:");
//                output.AppendLine($"[skip]{value}[/skip]");
//                return;
//            }
//            else
//            {
//                output.AppendLine($"ERROR: The argument '{args[0]}' is not a valid variable name.");
//                return;
//            }
//        }
//    }
//}
