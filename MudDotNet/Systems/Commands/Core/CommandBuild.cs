﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Linq;
//using System.Text;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "Build")]
//    public class CommandBuild : ACoreCommand
//    {
//        public CommandBuild() : base("build", EAccessLevel.Staff, 2, 3)
//        {
//        }

//        public string[] Types { get; } = { /*"exit", "item",*/ "room" };

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            DateTime now = DateTime.UtcNow;
//            IPlayer player = client.GetPlayer();

//            //output.AppendLine(key);
//            //output.AppendLine(switches.Join(", "));
//            //output.AppendLine(args.Join(", "));

//            string type = args[0];
//            string name = args[1];
//            string desc = (args.Count > 2 ? args[2] : string.Empty);
//            if (!this.Types.Contains(type))
//            {
//                output.Append($"ERROR: The '{this.Name}' command does not accept '{args[0]}' as a buildable type.");
//                return;
//            }

//            if (type == "room")
//            {
//                IRoom room = new Room();
//                room.CreationStamp = DateTime.UtcNow;
//                room.ID = this.EntitySystem.PopID();
//                room.Name = name;
//                room.Description = desc;
//                room.OwnerID = player.ID;
//                this.EntitySystem.Manager.Rooms.Add(room);
//                output.AppendLine($"You created {type} #{room.ID} named '{room.Name}'.");
//            }
//        }
//    }
//}
