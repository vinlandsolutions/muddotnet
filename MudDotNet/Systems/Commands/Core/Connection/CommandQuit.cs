﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Connection
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Quit")]
    public class CommandQuit : ACommand
    {
        public CommandQuit() : base("quit", EAccessLevel.Any, 0)
        {
        }

        public override void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
        {
            if (!client.Active) { output.Append("ERROR: You are already quiting."); }
            else
            {
                //if (client.EntityID > -1)
                //{
                //    IPlayer player = this.EntitySystem.Manager.Players.Get(client.EntityID);
                //    player.Client = null;
                //}
                client.Disconnect();
            }
        }
    }


}
