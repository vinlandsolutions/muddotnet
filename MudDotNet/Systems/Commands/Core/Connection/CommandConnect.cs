﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Text;
using CommonCore;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Connection
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Connect")]
    public class CommandConnect : ACoreCommand
    {
        public CommandConnect() : base("connect", EAccessLevel.Any, 2)
        {
        }

        public override void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
        {
            //IPlayer clientPlayer = this.EntitySystem.Manager.Players.Get(client.EntityID);
            //if (clientPlayer != null) { output.Append($"ERROR: You are already connected as '{clientPlayer.Name}'."); return; }
            if (!client.Account.IsNone()) { output.Append($"ERROR: You are already connected as '{client.Account.Alias}'."); return; }

            var account = context.AccountManager.Find(args[0], args[1]);
            if (account.IsNone()) { output.Append("ERROR: Player does not exist or password is incorrect."); return; }

            foreach (var other in context.Clients)
            {
                if (other.Account == account)
                {
                    output.Append("ERROR: Account is already online.");
                    return;
                }
            }

            client.Account = account;
            output.AppendFile(new FileInfo("./Data/welcome.txt"));
            return;

            //IPlayer player = this.EntitySystem.Manager.Players.FindMatch(args[0], false);
            //if (player == null) { output.Append("ERROR: Player does not exist or password is incorrect."); }
            //else if (!Util.ValidatePassword(args[1], player.Pass)) { output.Append("ERROR: Player does not exist or password is incorrect."); }
            ////else if (player != null && player.Online) { output.Append("ERROR: Player is already online."); }
            //else if (player.Banned) { output.Append("ERROR: Player is banned."); }
            //else
            //{
            //    if (player.Online)
            //    {
            //        var other = player.Client;
            //        player.Client = null;
            //        other.Disconnect();
            //    }

            //    if (player.ID == 0 && player.Access != EAccessLevel.Owner)
            //    {
            //        player.Access = EAccessLevel.Owner;
            //    }
            //    if (player.CreationStamp == DateTime.MinValue)
            //    {
            //        player.CreationStamp = DateTime.UtcNow;
            //    }

            //    player.Client = client;
            //    output.AppendFile(new FileInfo("./Data/welcome.txt"));
            //}
        }
    }
}
