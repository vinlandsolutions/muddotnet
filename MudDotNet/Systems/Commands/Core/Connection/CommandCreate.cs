﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.IO;
//using System.Text;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core.Connection
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "Create")]
//    public class CommandCreate : ACoreCommand
//    {
//        public CommandCreate() : base("create", EAccessLevel.Any, 2)
//        {
//        }

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            IPlayer clientPlayer = this.EntitySystem.Manager.Players.Get(client.EntityID);
//            if (null != clientPlayer)
//            { output.Append($"ERROR: You are already connected as '{clientPlayer.Name}'."); }
//            else if (null != this.EntitySystem.Manager.Players.FindMatch(args[0], false))
//            { output.Append($"ERROR: Player name '{args[0]}' is invalid or already in use."); }
//            else
//            {
//                IPlayer player = new Player();
//                player.CreationStamp = DateTime.UtcNow;
//                player.ID = this.EntitySystem.PopID();
//                player.OwnerID = player.ID;
//                player.Name = args[0];
//                player.Pass = Util.CreateHash(args[1]);
//                if (this.EntitySystem.Manager.Players.Count == 0)
//                {
//                    player.Access = EAccessLevel.Owner;
//                }
//                this.EntitySystem.Manager.Players.Add(player);
//                player.Client = client;
                
//                output.AppendFile(new FileInfo("./Data/welcome.txt"));
//            }
//        }
//    }
//}
