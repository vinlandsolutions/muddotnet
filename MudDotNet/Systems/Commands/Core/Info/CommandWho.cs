﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands.Core.Info
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Who")]
    public class CommandWho : ACoreCommand
    {
        public CommandWho() : base("who", EAccessLevel.Member)
        {
        }

        public override void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
        {
            output.AppendFormat(string.Format("{0,-20} {1,-15} {2,-5}", "Player Name", "IP", "Port"));
            output.AppendLine();
            foreach (IClient other in context.Clients)
            {
                //IPlayer player = this.EntitySystem.Manager.Players.Get(other.EntityID);
                output.AppendLine(this.GetLine(other));
            }
        }

        public string GetLine(IClient client)
        {
            return string.Format("{0,-20} {1,15} {2,5}", (client.Account == null ? "<Connecting>" : client.Account.Alias), client.Connection.IP, client.Connection.Port);
        }

        //public string GetLine(IClient client, IPlayer player)
        //{
        //    return string.Format("{0,-20} {1,15} {2,5}", (player == null ? "<Connecting>" : player.Name), client.Connection.IP, client.Connection.Port);
        //}
    }
}
