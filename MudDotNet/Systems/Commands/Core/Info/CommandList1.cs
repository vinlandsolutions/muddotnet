﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Text;
//using CommonCore;
//using MudDotNet.Networking;
//using MudDotNet.Systems.Help;

//namespace MudDotNet.Systems.Commands.Core.Info
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "List1")]
//    public class CommandList1 : ACommand
//    {
//        public CommandList1() : base("list", EAccessLevel.Any, 1)
//        {
//        }

//        public CommandSystem CommandSystem { get; protected set; }

//        public ISystemHelpProvider HelpSystem { get; protected set; }

//        public override object Initialize(ContextData context)
//        {
//            this.CommandSystem = context.SystemManager.GetSystem<CommandSystem>();
//            this.HelpSystem = context.SystemManager.GetSystem<ISystemHelpProvider>();
//            return null;
//        }

//        public override void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            output.Append("list1\n");
//        }
//    }
//}
