﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using CommonCore;
using MudDotNet.Networking;
using MudDotNet.Systems.Help;

namespace MudDotNet.Systems.Commands.Core.Info
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "Help")]
    public class CommandHelp : ACommand
    {
        public CommandHelp() : base("help", EAccessLevel.Any, -1)
        {
        }

        public ISystemHelpProvider Help { get; protected set; }

        public override object Initialize(ContextData context)
        {
            this.Help = context.SystemManager.GetSystem<ISystemHelpProvider>();
            return null;
        }

        public override void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
        {
            //result.Valid = true;
            if (args.Count == 0) { args = new[] { "topics" }; }
            HelpTopic topic = null;
            if (topic == null) { topic = this.Help.Topics.GetKeyTopic(args[0]); }
            if (topic == null) { topic = this.Help.Topics.SearchTopics(args); }

            if (topic != null)
            { output.AppendFormat(this.Help.Template, topic.Title, topic.Topic, topic.Content.ToString()); }
            else
            {
                var text = args.Join(" ").ToLower();
                output.Append($"Sorry, but '{text}' doesn't match any help topics.");
            }
        }
    }
}
