﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Text;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core.Info
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "Me")]
//    public class CommandMe : ACoreCommand
//    {
//        public CommandMe() : base("me", EAccessLevel.Any)
//        {
//        }

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            DateTime now = DateTime.UtcNow;

//            output.AppendLine("[repeat/]");
//            output.AppendLine($"[b][you/] ([iyou/], [ayou/])[/b]");
//            output.AppendLine($"[rjust,10]Socket[/rjust]: {client.Host}:{client.IP}:{client.Port}");
//            // ?Type, Flags?
//            //output.AppendLine("[repeat/]");

//            IPlayer player = client.GetPlayer();//this.EntitySystem.Manager.Players.Get(client.EntityID);
//            if (player != null)
//            {
//                output.AppendLine($"[rjust,10]Type[/rjust]: {player.Type}");

//                var spanCreation = new DateTimeSpan(player.CreationStamp, now);
//                output.AppendLine($"[rjust,10]Created[/rjust]: {player.CreationStamp.ToDefaultString()} ({spanCreation.ToDefaultString()})");

//                var spanConnection = new DateTimeSpan(player.ConnectionStamp, now);
//                output.AppendLine($"[rjust,10]Connected[/rjust]: {player.ConnectionStamp.ToDefaultString()} ({spanConnection.ToDefaultString()})");

//                var spanIdle = new DateTimeSpan(client.IdleStamp, now);
//                output.AppendLine($"[rjust,10]Idled[/rjust]: {client.IdleStamp.ToDefaultString()} ({spanIdle.ToDefaultString()})");

//                output.AppendLine();

//                IPlayer owner = this.EntitySystem.Manager.Players.Get(player.OwnerID);
//                if (owner == null) { output.AppendLine($"[rjust,10]Owner[/rjust]: <NONE> (#{player.OwnerID})"); }
//                else
//                {
//                    output.AppendLine($"[rjust,10]Owner[/rjust]: {owner.Name} (#{owner.ID}, {owner.Access})");
//                }

//                IPlayer parent = this.EntitySystem.Manager.Players.Get(player.ParentID);
//                if (parent == null) { output.AppendLine($"[rjust,10]Parent[/rjust]: <NONE> (#{player.ParentID})"); }
//                else
//                {
//                    output.AppendLine($"[rjust,10]Parent[/rjust]: {parent.Name} (#{parent.ID}, {parent.Access})");
//                }

//                IRoom home = this.EntitySystem.Manager.Rooms.Get(player.HomeID);
//                if (home == null) { output.AppendLine($"[rjust,10]Home[/rjust]: <NULL ROOM> (#{player.HomeID})"); }
//                else
//                {
//                    output.AppendLine($"[rjust,10]Home[/rjust]: {home.Name} (#{home.ID})");
//                }

//                IRoom location = this.EntitySystem.Manager.Rooms.Get(player.LocationID);
//                if (location == null) { output.AppendLine($"[rjust,10]Location[/rjust]: <NULL ROOM> (#{player.LocationID})"); }
//                else
//                {
//                    output.AppendLine($"[rjust,10]Location[/rjust]: {location.Name} (#{location.ID})");
//                }

//                output.AppendLine();

//                output.AppendLine(player.Description.IsEmpty() ? "<NONE>" : $"[skip]{player.Description}[/skip]");
//                output.AppendLine("[repeat/]");
//            }
//            else
//            {
//                output.AppendLine("[repeat/]");
//                var spanConnection = new DateTimeSpan(client.ConnectionStamp, now);
//                output.AppendLine($"[rjust,10]Connected[/rjust]: {spanConnection.ToDefaultString()} ({client.ConnectionStamp.ToString("yyyy-MM-dd H:mm:ss")})");

//                var spanIdle = new DateTimeSpan(client.IdleStamp, now);
//                output.AppendLine($"[rjust,10]Idled[/rjust]: {spanIdle.ToDefaultString()} ({client.IdleStamp.ToString("yyyy-MM-dd H:mm:ss")})");
//                output.AppendLine("[repeat/]");
//            }
//        }
//    }
//}
