﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Text;
//using CommonCore;
//using MudDotNet.Networking;

//namespace MudDotNet.Systems.Commands.Core.Info
//{
//    [Export(typeof(ICommand))]
//    [ExportMetadata("Title", "Look")]
//    public class CommandLook : ACoreCommand
//    {
//        public CommandLook() : base("look", EAccessLevel.Member, 0, 1)
//        {
//        }

//        public override void ProcessInput(IServer server, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
//        {
//            DateTime now = DateTime.UtcNow;
//            IPlayer clientPlayer = client.GetPlayer();

//            string target = (args.Count == 0 ? "here" : args[0].ToLower());
//            long targetID = EntityHelper.ParseLocalID(client, clientPlayer, target);
//            IEntity targetEntity = this.EntitySystem.Manager.GetEntity(targetID);
//            if (args.Count > 0 && targetEntity == null)
//            {
//                output.AppendLine($"ERROR: The first argument '{args[0]}' is not a valid target id.");
//                return;
//            }

//            if (targetEntity == null)
//            {
//                output.AppendLine("You see nothing.");
//                return;
//            }

//            output.AppendLine($"[b]{targetEntity.Name} (#{targetEntity.ID})[/b]");

//            string desc = targetEntity.Description;
//            if (desc.IsEmpty()) { desc = "<DESCRIPTION NOT SET>"; }
//            output.AppendLine(desc);

//            {
//                var itemContainer = targetEntity as IEntityItemContainer;
//                if (itemContainer != null)
//                {
//                    if (itemContainer.ItemIDs.Count == 0) { output.AppendLine("Contents: NONE"); }
//                    else
//                    {
//                        output.AppendLine("Contents:");
//                        foreach (var item in itemContainer.Items)
//                        {
//                            output.AppendLine($"\t{item.Name} (#{item.ID}{item.Type[0]})");
//                        }
//                    }
//                }
//            }

//            {
//                var playerContainer = targetEntity as IEntityPlayerContainer;
//                if (playerContainer != null)
//                {
//                    if (playerContainer.PlayerIDs.Count == 0) { output.AppendLine("Occupants: NONE"); }
//                    else
//                    {
//                        output.AppendLine("Occupants:");
//                        foreach (var player in playerContainer.Players)
//                        {
//                            output.AppendLine($"\t{player.Name} (#{player.ID}{player.Type[0]})");
//                        }
//                    }
//                }
//            }

//            {
//                var exitContainer = targetEntity as IEntityExitContainer;
//                if (exitContainer != null)
//                {
//                    if (exitContainer.ExitIDs.Count == 0) { output.AppendLine("Exits: NONE"); }
//                    else
//                    {
//                        output.AppendLine("Exits:");
//                        foreach (var exit in exitContainer.Exits)
//                        {
//                            output.AppendLine($"\t{exit.Name} (#{exit.ID}{exit.Type[0]})");
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
