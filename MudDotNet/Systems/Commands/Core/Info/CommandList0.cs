﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using CommonCore;
using MudDotNet.Networking;
using MudDotNet.Systems.Help;

namespace MudDotNet.Systems.Commands.Core.Info
{
    [Export(typeof(ICommand))]
    [ExportMetadata("Title", "List0")]
    public class CommandList0 : ACommand
    {
        public CommandList0() : base("list", EAccessLevel.Any, 1)
        {
        }

        public CommandSystem CommandSystem { get; protected set; }

        public ISystemHelpProvider HelpSystem { get; protected set; }

        public override object Initialize(ContextData context)
        {
            this.CommandSystem = context.SystemManager.GetSystem<CommandSystem>();
            this.HelpSystem = context.SystemManager.GetSystem<ISystemHelpProvider>();
            return null;
        }

        public override void ProcessInput(ContextData context, IClient client, string key, IList<string> switches, IList<string> args, StringBuilder output)
        {
            //output.Append("list0\n");
            if (args[0] == "commands")
            {
                output.AppendFormat("Found '{0}' Commands\n", this.CommandSystem.CommandManager.Count);
                foreach (var command in this.CommandSystem.CommandManager)
                {
                    output.AppendFormat("{0} ({1}, {2}, {3})\n", command.Name, command.Access, command.ArgCountMin, command.ArgCountMax);
                }
            }
            else if (args[0] == "helps")
            {
                output.AppendFormat("Found '{0}' Help Files\n", this.CommandSystem.CommandManager.Count);
                foreach (var topic in this.HelpSystem.Topics)
                {
                    output.AppendFormat("{0}\n", topic.Title);
                }
            }
        }
    }
}
