﻿namespace MudDotNet.Systems.Commands.Core
{
    public abstract class ACoreCommand : ACommand
    {
        protected ACoreCommand(string name, EAccessLevel access)
            : this(name, access, 0, 0, null) { }

        protected ACoreCommand(string name, EAccessLevel access, int argCount)
            : this(name, access, argCount, argCount, null) { }

        protected ACoreCommand(string name, EAccessLevel access, int argCount, string[] validSwitches)
            : this(name, access, argCount, argCount, validSwitches) { }

        protected ACoreCommand(string name, EAccessLevel access, int argCountMin, int argCountMax)
            : this(name, access, argCountMin, argCountMax, null) { }

        protected ACoreCommand(string name, EAccessLevel access, int argCountMin, int argCountMax, string[] validSwitches)
            : base(name, access, argCountMin, argCountMax, validSwitches) { }

        //protected EntitySystem EntitySystem { get; set; }

        public override object Initialize(ContextData context)
        {
            var result = base.Initialize(context);
            //this.EntitySystem = context.SystemManager.GetSystem<EntitySystem>();
            return result;
        }
    }
}
