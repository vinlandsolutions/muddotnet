﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonCore;

namespace MudDotNet.Systems.Commands
{
    public static partial class CommandHelper
    {
        private static readonly char[] _sepsSwitches = new char[] { '/' };
        public static void ParseCommand(StringBuilder input,
            out string key, out IList<string> switches, out IList<string> args,
            int argMaxCount = 0, ETokenRemainderAction remainderAction = ETokenRemainderAction.Discard)
        {
            Throw.If.Arg.Null(nameof(input), input);

            IList<string> tokens = Terminal.ParseTokens(input, argMaxCount: argMaxCount, remainderAction: remainderAction);
            //if (tokens.Count > 0) { args = tokens.Subset(1).ToList(); }
            args = tokens.Count == 0 ? null : tokens.Subset(1).ToList();

            if (tokens[0][0] != '/')
            {
                switches = tokens[0].Split(_sepsSwitches, StringSplitOptions.RemoveEmptyEntries).ToList();
                key = switches[0];
                switches.RemoveAt(0);
            }
            else
            {
                string temp = tokens[0];
                bool slash = true;
                int index = 0;
                for (; index < temp.Length; index++)
                {
                    if (slash && temp[index] != '/') { slash = false; }
                    else if (!slash && temp[index] == '/') { break; }
                }
                key = temp.Substring(0, index);
                switches = temp.Substring(index).Split(_sepsSwitches, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }
    }
}
