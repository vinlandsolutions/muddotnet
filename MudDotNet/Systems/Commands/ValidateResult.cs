﻿namespace MudDotNet.Systems.Commands
{
    public struct ValidateResult
    {
        public static ValidateResult Empty { get; } = new ValidateResult();

        public bool Valid { get; set; }

        public string Message { get; set; }

        public bool HasMessage => !string.IsNullOrEmpty(this.Message);
    }
}
