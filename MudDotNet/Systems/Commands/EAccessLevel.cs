﻿namespace MudDotNet.Systems.Commands
{
    public enum EAccessLevel : int
    {
        None,
        Any,
        Member,
        Staff,
        Admin,
        Owner
    }
}
