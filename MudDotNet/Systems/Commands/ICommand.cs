﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Collections.Generic;
using System.Text;
using MudDotNet.Networking;

namespace MudDotNet.Systems.Commands
{
    public interface ICommand
    {
        string Name { get; }

        EAccessLevel Access { get; }

        int ArgCountMin { get; }

        int ArgCountMax { get; }

        IReadOnlyList<string> ValidSwitches { get; }

        object? Initialize(ContextData context);

        StringBuilder ProcessInput(ContextData context, IClient client, StringBuilder input);
    }
}
