﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CommonCore;
using CommonCore.IO;
using MudDotNet.Networking;
using MudDotNet.Systems;

namespace MudDotNet
{
    public class Terminal : IDisposable
    {
        private static readonly object Lock = new object();

        public Terminal()
        {
            this.SystemInfo = SystemInfo.FromJsonFile(new FileInfo("./SystemInfo.json"));
            this.AccountManager = new AccountManager();
            this.SystemManager = new SystemManager()
                .Add(typeof(Terminal).Assembly)
                .Add(this.SystemInfo.SystemsDirectory, "*.Systems.*.dll");
            this.Server = new Server();

            this.Server.ServerInitialize += this.ServerInitialize;
            this.Server.ServerInitializing += this.ServerInitializing;
            this.Server.ServerInitialized += this.ServerInitialized;
            this.Server.ServerStarted += this.ServerStarted;
            this.Server.ServerStopped += this.ServerStopped;
            this.Server.ServerDestroy += this.ServerDestroy;
            this.Server.ServerDestroying += this.ServerDestroying;
            this.Server.ServerDestroyed += this.ServerDestroyed;

            this.Server.ClientConnected += this.ClientConnected;
            this.Server.ClientDisconnected += this.ClientDisconnected;
            this.Server.ClientInput += this.ClientInput;
            this.Server.ClientOutput += this.ClientOutput;
            this.Server.ClientUpdated += this.ClientUpdated;
        }

        public SystemInfo SystemInfo { get; }

        public IAccountManager AccountManager { get; }

        public ISystemManager SystemManager { get; }

        public IServer Server { get; protected set; }


        public ContextData GetContext() => new ContextData(this.SystemInfo, this.Server, this.AccountManager, this.SystemManager);








        private void ServerInitialize(object sender, ServerEventArgs e)
            => this.Server.Logger.WriteLine(nameof(ServerInitialize));

        private void ServerInitializing(object sender, ServerEventArgs e)
        {
            this.Server.Logger.LogInc("Account Manager Initializing");
            this.AccountManager.Load(this.SystemInfo.DataDirectory.GetFile("Accounts.json"));
            this.Server.Logger.Log("Found '{0}' Accounts", this.AccountManager.Count);
            this.Server.Logger.LogDec("Account Manager Initialized");

            this.Server.Logger.LogInc("System Manager Initializing");
            this.SystemManager.Compose();
            this.Server.Logger.Log("Found '{0}' Systems", this.SystemManager.Count);
            if (this.SystemManager.Count > 0)
            {
                var context = this.GetContext();
                foreach (var system in this.SystemManager)
                {
                    this.Server.Logger.LogInc("'{0}' Initializing", system.SystemTitle);
                    if (system is ISystemInitializable)
                    { ((ISystemInitializable)system).SystemInitialize(context); }
                    this.Server.Logger.LogDec("'{0}' Initialized", system.SystemTitle);
                }
            }
            this.Server.Logger.LogDec("System Manager Initialized");
        }

        private void ServerInitialized(object sender, ServerEventArgs e)
            => this.Server.Logger.Log(nameof(ServerInitialized));

        private void ServerStarted(object sender, ServerEventArgs e)
            => this.Server.Logger.Log(nameof(ServerStarted));

        private void ServerStopped(object sender, ServerEventArgs e)
            => this.Server.Logger.Log(nameof(ServerStopped));

        private void ServerDestroy(object sender, ServerEventArgs e)
            => this.Server.Logger.Log(nameof(ServerDestroy));

        private void ServerDestroying(object sender, ServerEventArgs e)
        {
            this.Server.Logger.Log(nameof(ServerDestroying));
            this.Server.Logger.LogInc("System Manager Finalizing");
            this.SystemManager.Compose();
            this.Server.Logger.Log("Found '{0}' Systems", this.SystemManager.Count);
            if (this.SystemManager.Count > 0)
            {
                var context = this.GetContext();
                foreach (var system in this.SystemManager)
                {
                    this.Server.Logger.LogInc("'{0}' Finalizing", system.SystemTitle);
                    if (system is ISystemFinalizeable)
                    { ((ISystemFinalizeable)system).SystemFinalize(context); }
                    this.Server.Logger.LogDec("'{0}' Finalized", system.SystemTitle);
                }
            }
            this.Server.Logger.LogDec("System Manager Finalized");
        }

        private void ServerDestroyed(object sender, ServerEventArgs e)
            => this.Server.Logger.WriteLine(nameof(ServerDestroyed));

        //this.Broadcast(null, client, new FileInfo("./Data/connection.txt"));
        private void ClientConnected(object sender, ClientEventArgs e)
        {
            var server = (IServer)sender;
            server.Logger.WriteLine(nameof(ClientConnected));
            server.Broadcast(null, e.Client, new FileInfo("./Data/connection.txt"));
        }

        private void ClientDisconnected(object sender, ClientEventArgs e)
        {
            var server = (IServer)sender;
            server.Logger.WriteLine(nameof(ClientDisconnected));
            server.Broadcast(null, e.Client, new FileInfo("./Data/disconnection.txt"));
        }

        private void ClientInput(object sender, ClientInputEventArgs e)
        {
            StringBuilder output = null;
            var handlers = this.SystemManager.GetSystems<ISystemInputProcessor>();
            if (handlers != null)
            {
                var context = this.GetContext();
                foreach (var handler in handlers)
                {
                    if (handler.HandlesInput)
                    {
                        object data = handler.CanProcessInput(context, e.Client, e.Input);
                        if (data != null)
                        {
                            output = handler.ProcessInput(context, e.Client, e.Input, data);
                            if (output != null) { break; }
                        }
                    }
                }
            }

            if (output != null) { this.Server.Broadcast(null, e.Client, output); }
            else { this.Server.Broadcast(null, e.Client, $"UNKOWN INPUT: {{{e.Input}}}"); }
        }

        private void ClientOutput(object sender, ClientOutputEventArgs e)
        {
            var handlers = this.SystemManager.GetSystems<ISystemOutputProcessor>();
            if (handlers != null)
            {
                var context = this.GetContext();
                foreach (var handler in handlers)
                {
                    if (handler.HandlesOutput)
                    {
                        object data = handler.CanProcessOutput(context, e.Sender, e.Receiver, e.Content);
                        if (data != null)
                        {
                            bool handled = handler.ProcessOutput(context, e.Sender, e.Receiver, e.Content, data);
                            if (handled) { break; }
                        }
                    }
                }
            }
            //output.ReplaceAny(Util.PuebloReplacements);
        }

        private void ClientUpdated(object sender, ClientEventArgs e)
            => Console.WriteLine(nameof(ClientUpdated));






























        #region IDisposable Support

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating if the current <see cref="ClientBus"/> instance
        /// has been disposed.
        /// </summary>
        public bool Disposed { get; protected set; } = false;

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose() => this.Dispose(true);

        /// <summary>
        /// Releases all resources used by the current instance of the <see cref="ClientBus"/> class.
        /// </summary>
        /// <param name="disposing">Indicate whether managed resources should be disposed.</param>
        protected virtual void Dispose(bool disposing)
        {
            lock (Terminal.Lock)
            {
                if (!this.Disposed)
                {
                    if (disposing)
                    {
                        this.Server.ServerInitialize -= this.ServerInitialize;
                        this.Server.ServerInitializing -= this.ServerInitializing;
                        this.Server.ServerInitialized -= this.ServerInitialized;
                        this.Server.ServerStarted -= this.ServerStarted;
                        this.Server.ServerStopped -= this.ServerStopped;
                        this.Server.ServerDestroy -= this.ServerDestroy;
                        this.Server.ServerDestroying -= this.ServerDestroying;
                        this.Server.ServerDestroyed -= this.ServerDestroyed;

                        this.Server.ClientConnected -= this.ClientConnected;
                        this.Server.ClientDisconnected -= this.ClientDisconnected;
                        this.Server.ClientInput -= this.ClientInput;
                        this.Server.ClientOutput -= this.ClientOutput;
                        this.Server.ClientUpdated -= this.ClientUpdated;

                        this.Server.Dispose(); this.Server = null;
                    }
                    this.Disposed = true;
                }
            }
        }

        #endregion




















        public static IList<string> ParseTokens(StringBuilder input,
            char delimiter = ' ', char quote = '"', char escape = '\\',
            int argSkipCount = 0, int argMaxCount = 0,
            ETokenRemainderAction remainderAction = ETokenRemainderAction.Discard)
            => ParseTokens(input.ToString(), delimiter, quote, escape, argSkipCount, argMaxCount, remainderAction);

        public static IList<string> ParseTokens(ReadOnlySpan<char> input,
            char delimiter = ' ', char quote = '"', char escape = '\\',
            int argSkipCount = 0, int argMaxCount = 0,
            ETokenRemainderAction remainderAction = ETokenRemainderAction.Discard)
        {
            if (input.IsEmpty) { return null; }
            ThrowHelper.IfArgLesser(argSkipCount, nameof(argSkipCount), 0);
            ThrowHelper.IfArgLesser(argMaxCount, nameof(argMaxCount), 0);

            bool quoted = false, escaped = false;
            var token = new StringBuilder();

            IList<string> args = new List<string>();
            int argCount = 0;
            for (int index = 0; index < input.Length; index++)
            {
                bool first = index == 0;
                bool last = index == input.Length - 1;

                char prev = (first ? '\0' : input[index - 1]);
                char current = input[index];
                char next = (last ? '\0' : input[index + 1]);

                if (current == escape && (next == quote || next == escape)) { escaped = true; continue; }
                else if (!escaped && !quoted && current == quote && (first || prev == delimiter)) { quoted = true; continue; }
                else if (!escaped && quoted && current == quote && (last || next == delimiter)) { quoted = false; continue; }
                else if (escaped || quoted || current != delimiter)
                {
                    token.Append(current);
                    escaped = false;
                    continue;
                }
                else if (!escaped && !quoted && current == delimiter)
                {
                    if (!token.IsEmpty())
                    {
                        argCount += 1;
                        if (argCount > argSkipCount)
                        {
                            args.Add(token.ToString());
                            if (argMaxCount > 0 && args.Count == argMaxCount)
                            {
                                if (remainderAction == ETokenRemainderAction.Add)
                                {
                                    var remainder = input.Slice(index);
                                    args.Add(remainder.Trim(delimiter).ToString());
                                    return args;
                                }
                                else if (remainderAction == ETokenRemainderAction.Join)
                                {
                                    var remainder = input.Slice(index);
                                    args[args.Count - 1] += remainder.ToString();
                                }
                                return args;
                            }
                        }
                        token.Clear();
                    }
                    continue;
                }
            }

            if (!token.IsEmpty())
            {
                argCount += 1;
                args.Add(token.ToString());
            }
            return args;
        }
    }
}
