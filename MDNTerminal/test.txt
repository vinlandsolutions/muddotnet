﻿string text = content.Trim();
int ldiff = content.Length - content.TrimStart().Length;
int rdiff = content.Length - content.TrimEnd().Length;
string output = string.Empty;
if (ldiff > 0) { output += new string(' ', ldiff); }
output += $"\x1B[6z<send href=\"help {text}\" hint=\"Find help for {text}\">{text}</send>\x1B[7z";
if (rdiff > 0) { output += new string(' ', rdiff); }
return output;