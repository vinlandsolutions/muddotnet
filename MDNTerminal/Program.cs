﻿using System;
using System.Threading;
using MudDotNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MDNTerminal
{
    class Program
    {
        static void Main(string[] args)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Converters = { new StringEnumConverter(), new FileSystemInfoConverter() }
            };
            //CSScript.Evaluator.ReferenceDomainAssemblies();

            Console.WriteLine("++ Terminal");
            using (var terminal = new Terminal())
            {
                terminal.Server.StartAsync(terminal.SystemInfo.PortNumber, terminal.SystemInfo.BacklogSize);
                while (!terminal.Server.Running) { }
                while (terminal.Server.Running)
                {
                    if (Console.KeyAvailable)
                    {
                        var key = Console.ReadKey(true);
                        terminal.Server.Stop();
                    }
                    Thread.Sleep(1000);
                }
            }
            Console.WriteLine("++ Terminal End");

            {
                //var subject = "a\nb".AsSegment();
                //var expected = new[] { "a", "b" };
                //foreach (var line in subject.ToLineSequence())
                //{
                //    Console.WriteLine(line);
                //}
            }

            {
                //string s = "a\nb\r\nc\rd\n\re";//File.ReadAllText("./Data/Help/Content/topics.help");
                //var r = new StringReader(s);
                //string line;
                //while ((line = r.ReadLine()) != null)
                //{
                //    Console.WriteLine(line);
                //}
            }

            {
                //var tags = new TagInfo();
                //tags.Blocks = new List<string[]>();

                //tags.Blocks.Add(new[] { "create", "\x1B[6z<create>", "</create>\x1B[7z", null, null });
                //tags.Blocks.Add(new[] { "connect", "\x1B[6z<connect>", "</connect>\x1B[7z", null, null });

                //tags.Blocks.Add(new[] { "player", "\x1B[6z<player>", "</player>\x1B[7z", null, null });
                //tags.Blocks.Add(new[] { "room", "\x1B[6z<room>", "</room>\x1B[7z", null, null });

                //var json = JsonConvert.SerializeObject(tags, Formatting.Indented);
                //Console.WriteLine(json);
            }

            {
                //                var builder = new StringBuilder();
                //                //builder.AppendFile(new FileInfo("./Data/Help/topics.help"));
                //                builder.Append(
                //@"#
                //# test comment
                //## escape comment
                //#id
                //#id topics
                //#title Help Topic Index
                //#title test
                //#topic Main Index of the Help System
                //#keywords topic, topics, index, list, main, system

                //Welcome to the main index of the Help System. Below are a list of main topics that might interest you, each providing an overview of the related topic and an index of sub topics.

                //    [help][ljust,25]Commands[/ljust][/help][red][help][ljust,25]Entities[/ljust][/help][/red][help][ljust,25]Formatting[/ljust][/help]
                //");
                //                //PragmaParser.Parse(builder, out PragmaData data, out StringBuilder content);
                //                //Console.Write(content.ToString());
                //                var topic = new HelpTopic();
                //                topic.Load(builder);
                //                Console.Write(topic.Content);
            }

            {
                //SecureString fget(string text)
                //{
                //    var secret = new SecureString();
                //    foreach (var c in text) { secret.AppendChar(c); }
                //    return secret;
                //}

                //var alias = "admin";
                //var accounts = new AccountManager();
                //using (var secrect = fget("password")) { accounts.Add(alias, secrect); }
                ////accounts.Save(new FileInfo("./Accounts.json"));
                //var json = JsonConvert.SerializeObject(accounts.Accounts, Formatting.Indented);
                //Console.WriteLine(json);
                //var account = accounts.Find(alias);
                //Console.WriteLine(account);
                //using (var secrect = fget("password")) { account = accounts.Find(alias, secrect); }
                //Console.WriteLine(account);
            }


            {
                //var text = new StringBuilder("test \\arg1 ar\\\\g2 \"some arg 3\" \"some \\\"arg\\\" 4\"");
                //Console.WriteLine(text);
                //IList<string> tokens;

                //tokens = Terminal.ParseTokens(text);
                //for (int index = 0; index < tokens.Count; index++)
                //{
                //    Console.WriteLine("{0,3}: '{1}'", index, tokens[index]);
                //}
                //Console.WriteLine();

                //int start = 0;
                //int count = 0;

                //start = 1;
                //count = 3;

                //Console.WriteLine("Start at '{0}', take '{1}'", start, count);
                //foreach (ETokenRemainderAction action in Enum.GetValues(typeof(ETokenRemainderAction)))
                //{
                //    tokens = Terminal.ParseTokens(text, ' ', '"', '\\', start, count, action);
                //    Console.WriteLine($"{action}: {tokens.Count} - {tokens.Join(", ")}");
                //}
                //Console.WriteLine();

                //start = 2;
                //count = 2;

                //Console.WriteLine("Start at '{0}', take '{1}'", start, count);
                //foreach (ETokenRemainderAction action in Enum.GetValues(typeof(ETokenRemainderAction)))
                //{
                //    tokens = Terminal.ParseTokens(text, ' ', '"', '\\', start, count, action);
                //    Console.WriteLine($"{action}: {tokens.Count} - {tokens.Join(", ")}");
                //}
                //Console.WriteLine();
            }
        }
    }
}
