﻿#id formatting
#title BBTag Formatting Topic Index
#topic Using Tags to Format Text
#keywords format, formats, formatting, tag, tags, bbtag, bbcode, style, styles, styling, color, colors, coloring, bold, underline, hilite, highlight, hilight, index, list, main, system, input, output

The BBTag Formatting System provides a simple method of formatting and manipulating text for display to users. Everything that users receive from the server will be processed for formatting codes before being sent to users. The system is based on the psuedo-HTML-like tags used on many forums and comment sections around the internet.

NOTE: While many bbtags provide functionality similar to softcode substitutions and function available on legacy mush systems, this formatting system is not intended and will not function well as a softcode replacement. For true power, see the Scripting topic.

NOTE: Left brackets must be escaped (\\\[) if they do not form part of a valid open, close, or void tag. Dangling tags and not existent tags will be rendered at plain text.

Tags can be broken down into a few simple patterns (all brackets are literal in these patterns):

[b]Pattern 1[/b]: [<key>/]
[b]Pattern 1[/b]: [<key>,arg0,...,argN/]
[b]Pattern 1[/b]: [<key>]<content>[/<key>]
[b]Pattern 1[/b]: [<key>,arg0,...,argN]<content>[/<key>]

[b]key[/b]: Each tag is denoted by unique text, known as the tag key. Keys can not contain commas (,), left or right brackets (\[]), or slashes (/).

[b]content[/b]: Block tags can contain any nested or non-nested mixture of literal text, block tags, void tags, dangling open or close tags, and escaped left brackets. In other words, all text between block tags should behave the same as it would outside any tags.

[b]args[/b]: Arguments are a comma (,) separated list of values that can contain any character other than commas (,), left or right brackets (\[]), and slashes (/) and allows control over how the tag produces its output. To include one of those symbols as part of an argument, simply prefix the backslash (\) to the symbol. Not all tags accept arguments and no tag should require arguments.

