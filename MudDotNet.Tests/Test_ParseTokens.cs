using System;
using FluentAssertions;
using Xunit;

namespace MudDotNet.Tests
{
    public class Test_ParseTokens
    {
        [Fact]
        public void Test1()
        {
            var subject = "test \\arg1 ar\\\\g2 \"some arg 3\" \"some \\\"arg\\\" 4\"";
            var expected = new[] { "test", "\\arg1", "ar\\g2", "some arg 3", "some \"arg\" 4" };
            Terminal.ParseTokens(subject).Should().BeEquivalentTo(expected);
        }
    }
}
