﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using FluentAssertions;
//using MudDotNet.Experimental;
//using Xunit;

//namespace MudDotNet.Tests
//{
//    public class Test_TextSegment
//    {
//        [Theory]
//        [InlineData("a\r\nb", new[] { "a", "b" })]
//        [InlineData("a\n\rb", new[] { "a", "b" })]
//        [InlineData("a\nb", new[] { "a", "b" })]
//        [InlineData("\nb", new[] { "", "b" })]
//        [InlineData("a\n", new[] { "a", "" })]
//        [InlineData("\n", new[] { "", "" })]
//        [InlineData("", new[] { "" })]
//        [InlineData("abc", new[] { "abc" })]
//        public void ToLineSequence(string subject, string[] expected)
//            => subject.AsSegment().ToLineSequence().Should().BeEquivalentTo(expected.Select(s => s.AsSegment()));
//    }
//}
